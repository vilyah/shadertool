/*
    Exported using GLSL Studio: 26/02/2015 09:04
    Name: Volume Raymarching
    Author: Vil
*/

precision highp float;

#define M_PI 3.1415926535897932384626433832795

//
// GLSL textureless classic 3D noise "cnoise",
// with an RSL-style periodic variant "pnoise".
// Author:  Stefan Gustavson (stefan.gustavson@liu.se)
// Version: 2011-10-11
//
// Many thanks to Ian McEwan of Ashima Arts for the
// ideas for permutation and gradient selection.
//
// Copyright (c) 2011 Stefan Gustavson. All rights reserved.
// Distributed under the MIT license. See LICENSE file.
// https://github.com/ashima/webgl-noise
//

vec3 mod289(vec3 x)
{
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x)
{
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x)
{
  return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}

vec3 fade(vec3 t) {
  return t*t*t*(t*(t*6.0-15.0)+10.0);
}

// Classic Perlin noise
float cnoise(vec3 P)
{
  vec3 Pi0 = floor(P); // Integer part for indexing
  vec3 Pi1 = Pi0 + vec3(1.0); // Integer part + 1
  Pi0 = mod289(Pi0);
  Pi1 = mod289(Pi1);
  vec3 Pf0 = fract(P); // Fractional part for interpolation
  vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0
  vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
  vec4 iy = vec4(Pi0.yy, Pi1.yy);
  vec4 iz0 = Pi0.zzzz;
  vec4 iz1 = Pi1.zzzz;

  vec4 ixy = permute(permute(ix) + iy);
  vec4 ixy0 = permute(ixy + iz0);
  vec4 ixy1 = permute(ixy + iz1);

  vec4 gx0 = ixy0 * (1.0 / 7.0);
  vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - 0.5;
  gx0 = fract(gx0);
  vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
  vec4 sz0 = step(gz0, vec4(0.0));
  gx0 -= sz0 * (step(0.0, gx0) - 0.5);
  gy0 -= sz0 * (step(0.0, gy0) - 0.5);

  vec4 gx1 = ixy1 * (1.0 / 7.0);
  vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - 0.5;
  gx1 = fract(gx1);
  vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
  vec4 sz1 = step(gz1, vec4(0.0));
  gx1 -= sz1 * (step(0.0, gx1) - 0.5);
  gy1 -= sz1 * (step(0.0, gy1) - 0.5);

  vec3 g000 = vec3(gx0.x,gy0.x,gz0.x);
  vec3 g100 = vec3(gx0.y,gy0.y,gz0.y);
  vec3 g010 = vec3(gx0.z,gy0.z,gz0.z);
  vec3 g110 = vec3(gx0.w,gy0.w,gz0.w);
  vec3 g001 = vec3(gx1.x,gy1.x,gz1.x);
  vec3 g101 = vec3(gx1.y,gy1.y,gz1.y);
  vec3 g011 = vec3(gx1.z,gy1.z,gz1.z);
  vec3 g111 = vec3(gx1.w,gy1.w,gz1.w);

  vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
  g000 *= norm0.x;
  g010 *= norm0.y;
  g100 *= norm0.z;
  g110 *= norm0.w;
  vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
  g001 *= norm1.x;
  g011 *= norm1.y;
  g101 *= norm1.z;
  g111 *= norm1.w;

  float n000 = dot(g000, Pf0);
  float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
  float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
  float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
  float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
  float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
  float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
  float n111 = dot(g111, Pf1);

  vec3 fade_xyz = fade(Pf0);
  vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
  vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
  float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x); 
  return 2.2 * n_xyz;
}

// Classic Perlin noise, periodic variant
float pnoise(vec3 P, vec3 rep)
{
  vec3 Pi0 = mod(floor(P), rep); // Integer part, modulo period
  vec3 Pi1 = mod(Pi0 + vec3(1.0), rep); // Integer part + 1, mod period
  Pi0 = mod289(Pi0);
  Pi1 = mod289(Pi1);
  vec3 Pf0 = fract(P); // Fractional part for interpolation
  vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0
  vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
  vec4 iy = vec4(Pi0.yy, Pi1.yy);
  vec4 iz0 = Pi0.zzzz;
  vec4 iz1 = Pi1.zzzz;

  vec4 ixy = permute(permute(ix) + iy);
  vec4 ixy0 = permute(ixy + iz0);
  vec4 ixy1 = permute(ixy + iz1);

  vec4 gx0 = ixy0 * (1.0 / 7.0);
  vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - 0.5;
  gx0 = fract(gx0);
  vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
  vec4 sz0 = step(gz0, vec4(0.0));
  gx0 -= sz0 * (step(0.0, gx0) - 0.5);
  gy0 -= sz0 * (step(0.0, gy0) - 0.5);

  vec4 gx1 = ixy1 * (1.0 / 7.0);
  vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - 0.5;
  gx1 = fract(gx1);
  vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
  vec4 sz1 = step(gz1, vec4(0.0));
  gx1 -= sz1 * (step(0.0, gx1) - 0.5);
  gy1 -= sz1 * (step(0.0, gy1) - 0.5);

  vec3 g000 = vec3(gx0.x,gy0.x,gz0.x);
  vec3 g100 = vec3(gx0.y,gy0.y,gz0.y);
  vec3 g010 = vec3(gx0.z,gy0.z,gz0.z);
  vec3 g110 = vec3(gx0.w,gy0.w,gz0.w);
  vec3 g001 = vec3(gx1.x,gy1.x,gz1.x);
  vec3 g101 = vec3(gx1.y,gy1.y,gz1.y);
  vec3 g011 = vec3(gx1.z,gy1.z,gz1.z);
  vec3 g111 = vec3(gx1.w,gy1.w,gz1.w);

  vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
  g000 *= norm0.x;
  g010 *= norm0.y;
  g100 *= norm0.z;
  g110 *= norm0.w;
  vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
  g001 *= norm1.x;
  g011 *= norm1.y;
  g101 *= norm1.z;
  g111 *= norm1.w;

  float n000 = dot(g000, Pf0);
  float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
  float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
  float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
  float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
  float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
  float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
  float n111 = dot(g111, Pf1);

  vec3 fade_xyz = fade(Pf0);
  vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
  vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
  float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x); 
  return 2.2 * n_xyz;
}


// -------------

float fbm(vec3 pos, int octaves, float lacunarity, float gain)
{
  float sum = 0.0;
  float amp = 1.0;
  vec3 p = pos;

  for (int i = 0; i < octaves; ++i) {
    amp *= gain;
    sum += amp * cnoise(p);
    p *= lacunarity;
  }

  return sum;
}


float normalized_fbm(vec3 pos, int octaves, float lacunarity, float gain)
{
  float sum = 0.0;
  float amp = 1.0;
  vec3 p = pos;

  for (int i = 0; i < octaves; ++i) {
    amp *= gain;
    sum += amp * (cnoise(p) * 0.5 + 0.5);
    p *= lacunarity;
  }

  return sum;
}


// -------------

float sdSphere(vec3 pos, float r)
{
  return length(pos) - r;
}


float sdScene(vec3 pos)
{
  return sdSphere(pos, sin(iGlobalTime) * 0.25 + 1.0);
}


/*
float hitScene(vec3 pos, vec3 dir, float maxT)
{
  // TODO
}
*/


float fmod(float x, float y)
{
  return fract(x / y) * y;
}


float density(float d, vec3 pos)
{
  if (d >= 0.0) {
    return 0.0;
  }

  vec3 ofs = vec3(fmod(iGlobalTime * 0.25, 60.0)) * vec3(1.0, -1.3, 0.8);
  pos += ofs;

  float dn = 0.0;
  dn += (cnoise(pos                ) * 0.5 + 0.5) * 0.2;
  dn += (cnoise(pos * pow(1.9, 1.0)) * 0.5 + 0.5) * 0.3;
  dn += (cnoise(pos * pow(1.9, 2.0)) * 0.5 + 0.5) * 0.5;
  dn += (cnoise(pos * pow(1.9, 3.0)) * 0.5 + 0.5) * 0.5;

  // Fade the density out towards the edge of the volume.
  float shell = 0.3;
  dn *= pow(min(-d / shell, 1.0), 2.0);

  return dn;
}


float transmittance(vec3 pos, vec3 dir, float maxT, int samples, float absorption)
{
  float dt = maxT / float(samples - 1);
  float rhomult = -absorption * dt;
  float Tr = 1.0;
  vec3 p = pos;
  vec3 d = dir * dt;
  for (int i = 0; i < samples; ++i) {
    p += d;
    float rho = density(sdScene(p), p);
    Tr *= exp(rhomult * rho);
  }
  return Tr;
}


float phase(vec3 Li, vec3 Lo)
{
  // Isotropic phase function, independent of angle between the two directions:
  //return 1.0 / (4.0 * M_PI);
  //return 1.0;

  // Henyey-Greenstein phase function:
  float g = 0.5; // must be between -1 and 1
  float gsqr = g * g;
  return (1.0 - gsqr) / (4.0 * M_PI * pow(1.0 + gsqr - 2.0 * g * dot(Li, Lo), 1.5));
}


// d = signed distance from the edge of the volume.
// rho = density of the volume
vec3 volumeColor(float d, float rho)
{
  vec3 flame = vec3(1.0, 0.4, 0.2);
  vec3 smoke = vec3(0.6);

  if (d < -0.5) {
    return flame;
  }
  else if (d < -0.3) {
    return mix(flame, smoke, (d + 0.5) * 5.0);
  }
  else if (d < 0.0) {
    return smoke;
  }
  else {
    return vec3(0.0);
  }
}


vec3 lookAt(vec3 pos, vec3 target, vec3 up, float fov, in vec2 fragCoord)
{
  vec2 uv = (fragCoord.xy / iResolution.xy) * 2.0 - 1.0;
  float imgU = atan(fov) * distance(pos, target);
  float imgV = imgU * iResolution.y / iResolution.x;
  vec3 right = cross(normalize(target - pos), up);
  return normalize(target + right * imgU * uv.x + up * imgV * uv.y - pos);
}


void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
  vec3 pos = vec3(0.0, 0.0, 3.0);
  vec3 dir = lookAt(pos, vec3(0.0), vec3(0.0, 1.0, 0.0), radians(60.0), fragCoord);
  vec3 lightPos = vec3(2.0);
  vec3 lightColor = vec3(20.0);

  float eps = 0.001;

  float t = 1.0;
  float maxT = 3.0;
  int viewSamples = 10;
  int lightSamples = 5;
  float absorption = 2.0;

  vec3 color = vec3(0.0);
  float opacity = 0.0;
  float trans = 1.0;
  pos += dir * t;
  maxT -= t;

  float dt = maxT / float(viewSamples);
  float rhomult = -absorption * dt;
  float Tr = 1.0;
  for (int i = 0; i < viewSamples; ++i) {
    pos += dir * dt;

    float d = sdScene(pos);
    float rho = density(d, pos);
    float Ti = exp(rhomult * rho);
    Tr *= Ti;

    vec3 lightDir = normalize(lightPos - pos);
    float lightDist = min(abs(d), distance(lightPos, pos));

    float Tl = transmittance(pos, lightDir, lightDist, lightSamples, absorption);
    vec3 Li = Tl * lightColor * phase(-dir, lightDir);

    color += Tr * Li * volumeColor(d, rho) * rho * dt;
    opacity += (1.0 - Ti) * (1.0 - opacity);
  }

  //vec3 sky = pow(vec3(1.0 - fragCoord.y / iResolution.y), vec3(1.6, 1.4, 0.9));
  //color += sky * (1.0 - opacity);

  fragColor = vec4(color, 1.0);
}

