// A library of useful shader functions.
//
// For now, the only way to reuse them is to copy & paste them into your code.


#define M_PI 3.1415926535897932384626433832795


// Convert a color from the HSV colour space to RGB
//
// This is from http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}


// This is from iq's "Mike" example.
float hash( float n )
{
  return fract(sin(n)*43758.5453123);
}


// This is from iq's "Mike" example.
float noise( in float x )
{
  float p = floor(x);
  float f = fract(x);

  f = f*f*(3.0-2.0*f);

  return mix( hash(p+0.0), hash(p+1.0),f);
}


// This is from iq's "Mike" example.
float noise( in vec2 x )
{
    vec2 p = floor(x);
    vec2 f = fract(x);

    f = f*f*(3.0-2.0*f);

    float n = p.x + p.y*157.0;

    return mix(mix( hash(n+  0.0), hash(n+  1.0),f.x),
               mix( hash(n+157.0), hash(n+158.0),f.x),f.y);
}


// This is from iq's "Mike" example.
const mat2 m2 = mat2( 0.80, -0.60, 0.60, 0.80 );


// This is from iq's "Mike" example.
float fbm( vec2 p )
{
    float f = 0.0;

    f += 0.5000*noise( p ); p = m2*p*2.02;
    f += 0.2500*noise( p ); p = m2*p*2.03;
    f += 0.1250*noise( p ); p = m2*p*2.01;
    f += 0.0625*noise( p );

    return f/0.9375;
}


// Given a point on the surface of a unit sphere centred at the origin, return
// the corresponding UV coordinates.
vec2 sphereUV(vec3 pos)
{
  return vec2( 0.5 + atan(pos.z, pos.x) / (2 * M_PI),
               0.5 - asin(pos.y) / M_PI );
}

