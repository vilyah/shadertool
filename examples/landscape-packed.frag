#define V vec4
void mainImage(out vec4 fragColor, in vec2 fragCoord){vec2 uv=fragCoord.xy/iResolution.xy;uv.x+=iGlobalTime;uv.x*=0.67;V v[3]=V[](V(uv.x+4.1,0.1,3.3,0.02),V(uv.x+1.1,0.2,3.3,0.05),V(uv.x,0.7,3.3,0.10));vec3 c=pow(vec3(0.3,0.3,0.9),vec3(uv.y));for(int i=0;i<3;++i){V r = v[i];for(int i=0;i<5;++i){r.y+=sin(r.x*r.z)*r.w;r.z*=1.7;r.w*=0.5;}if(uv.y<r.y)c-=vec3(0.2);}fragColor=V(c,1.0);}
