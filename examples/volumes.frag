// Raymarching parameters
const int maxViewSteps = 64;
const int maxLightSteps = 32;
const float epsilon = 1e-3;
const float minStep = 0.0001;

// Materials
struct Material {
  vec3 color;
  vec3 absorption;
  vec3 scattering;
};

// One entry per object ID.
const Material materials[3] = Material[](
  Material(vec3(0.76, 0.74, 0.88), vec3(0.0), vec3(0.0)), // background
  Material(vec3(0.67, 0.45, 0.45), vec3(0.5), vec3(0.5)), // box - sphere
  Material(vec3(0.97, 0.95, 0.45), vec3(1.0), vec3(1.0))  // inner sphere
);


struct Hit {
  vec3 pos;
  int obj;
  int numSteps;
};


// ------- Signed distance functions -------

// Signed distance to a sphere of radius r centred at the origin.
float sdSphere(vec3 pos, float r)
{
  return length(pos) - r;
}


// Signed distance to a plane described by the equation:
//   pos.x * plane.x + pos.y * plane.y + pos.z * plane.z + plane.w == 0;
// plane must be normalized.
float sdPlane(vec3 pos, vec4 plane)
{
  return dot(pos, plane.xyz) + plane.w;
}


// Signed distance to an axis-aligned box centred at the origin, with side
// lengths s.
float sdBox(vec3 pos, vec3 s)
{
  vec3 d = abs(pos) - s;
  return min(max(d.x, max(d.y, d.z)), 0.0) + length(max(d, 0.0));
}


// Subtract distance field d2 from distance field d1.
float opSubtract(float d1, float d2)
{
  return max(d1, -d2);
}


vec3 domRepeat(vec3 pos, vec3 period)
{
  vec3 halfPeriod = period * 0.5;
  return mod(pos + halfPeriod, period) - halfPeriod;
}


// ------ The scene interface ------

// pos: position in world space.
// return value: x = signed distance, y = object ID
vec2 sceneDist(vec3 pos)
{
  float minSignedDist = 1e20;
  int objectID = 0;

  float signedDist;

  // box - sphere
  float s = sin(iGlobalTime);
  float c = cos(iGlobalTime);
  mat3 rotX = mat3( 1.0, 0.0, 0.0,
                    0.0,   c,   s,
                    0.0,  -s,   c );
  mat3 rotY = mat3(   c, 0.0,   s,
                    0.0, 1.0, 0.0,
                     -s, 0.0,   c );
  vec3 tmpPos = rotY * rotX * domRepeat(pos, vec3(5.0));
  float d1 = sdBox(tmpPos, vec3(0.4, 0.4, 0.4));
  float d2 = sdSphere(tmpPos, 0.5);
  signedDist = opSubtract(d1, d2);
  if (signedDist < minSignedDist) {
    minSignedDist = signedDist;
    objectID = 1;
  }

  // Inner sphere
  signedDist = sdSphere(tmpPos, 0.2 + sin(iGlobalTime * 3.0) * 0.1);
  if (signedDist < minSignedDist) {
    minSignedDist = signedDist;
    objectID = 2;
  }

  return vec2(minSignedDist, float(objectID));
}


vec3 sceneNormal(Hit hit)
{
  vec3 dx = vec3(epsilon, 0.0, 0.0);
  vec3 dy = vec3(0.0, epsilon, 0.0);
  vec3 dz = vec3(0.0, 0.0, epsilon);
  return normalize(vec3( sceneDist(hit.pos + dx).x - sceneDist(hit.pos - dx).x,
                         sceneDist(hit.pos + dy).x - sceneDist(hit.pos - dy).x,
                         sceneDist(hit.pos + dz).x - sceneDist(hit.pos - dz).x ));
}


// ------- Raycasting -------

// Cast a ray into the scene and return the position & object ID of the first hit.
//
// If we don't hit anything, the return value will have the xyz components set
// to the ray direction and w component set to -1 (the ID of the background).
Hit intersect(vec3 pos, vec3 dir, float minT, float maxT, int maxSteps)
{
  float t = minT;
  Hit hit = Hit(dir, -1, 0);
  for (int i = 0; i < maxSteps; ++i) {
    vec3 p = pos + dir * t;
    vec2 d = sceneDist(p);
    float unsignedDist = abs(d.x);
    if (unsignedDist < epsilon) {
      hit.pos = p;
      hit.obj = int(d.y);
      break;
    }
    t += max(unsignedDist, 0.001);
    ++hit.numSteps;
    if (t >= maxT) {
      break;
    }
  }

  return hit;
}


vec3 lookAt(in vec3 pos, in vec3 target, in vec3 up, float fovRadians, in vec2 fragCoord)
{
  vec2 uv = (fragCoord.xy / iResolution.xy) * 2.0 - 1.0;
  vec3 camDir = normalize(target - pos);
  vec3 camRight = normalize(cross(camDir, up));
  float imgU = atan(fovRadians) * distance(pos, target);
  float imgV = imgU * iResolution.y / iResolution.x;
  return normalize(target + camRight * imgU * uv.x + up * imgV * uv.y - pos);
}


#define NUM_SAMPLES 64

void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
  vec3 pos = vec3(cos(iGlobalTime), 1.6 + sin(iGlobalTime) * 0.5, 3.0);
  vec3 dir = lookAt(pos, vec3(0.0, 0.5, 0.0), vec3(0.0, 1.0, 0.0), radians(60.0), fragCoord);
  vec3 light = vec3(5.0);

  // Keep marching until we reach a solid surface.
  Hit hit = intersect(pos, dir, 0.0, 200.0, maxViewSteps);
  Material mat = materials[hit.obj];

  // Now march through it, evaluating the volume equation at each step.

  vec3 color = mat.color;
  bool showStepCount = false;
  if (!showStepCount) {
    if (hit.obj > 0) {
      // Shade the intersection point.
      vec3 N = sceneNormal(hit);
      vec3 L = normalize(light - hit.pos);
      float NdotL = max(dot(N, L), 0.0);

      Hit lightHit = intersect(hit.pos, L, 3.0 * epsilon, 200.0, maxLightSteps);
      float lightAmount = (lightHit.obj >= 0) ? 0.0 : 1.0;

      color *= NdotL * lightAmount;
    }
  }
  else {
    color = mix(vec3(0.0, 0.0, 1.0), vec3(1.0, 0.0, 0.0), float(hit.numSteps) / float(maxViewSteps - 1));
  }
  fragColor = vec4(color, 1.0);
}
