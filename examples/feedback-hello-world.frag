void mainImage(out vec4 fragColor, out vec4 feedback, in vec2 fragCoord) {
  vec2 dUV = 1.0 / iResolution.xy;
  vec2 dU = vec2(dUV.x, 0.0);
  vec2 dV = vec2(0.0, dUV.y);
  vec2 uv = fragCoord * dUV;

  // int numPixels = int(iResolution.x * iResolution.y);
  const int modulus = int(iResolution.x * 3.141 * 27);

  int pixelNum = int(fragCoord.y * iResolution.x + fragCoord.x) % modulus;
  int timeNum = int(iGlobalTime * 1391.0 * iResolution.x) % modulus;

  // Count the active neighbours
  bool isActive = texture(iChannelFeedback, uv).x > 0.0;
  int numActive = 0;
  if (texture(iChannelFeedback, uv + dU).x > 0.0) ++numActive;
  if (texture(iChannelFeedback, uv - dU).x > 0.0) ++numActive;
  if (texture(iChannelFeedback, uv + dV).x > 0.0) ++numActive;
  if (texture(iChannelFeedback, uv - dV).x > 0.0) ++numActive;

  fragColor = isActive ? vec4(1.0) : vec4(0.0);

  // Decide whether this cell should remain active.
  feedback = ((numActive > 0 && numActive < 4) || (timeNum == pixelNum)) ? vec4(1.0) : vec4(0.0);
}
