#define M_PI     3.14159265358979
#define M_INV_PI 0.31830988618379


vec2 uvToLatLong(vec2 uv)
{
  return (uv - 0.5) * vec2(2.0, 1.0) * M_PI;
}


vec3 latLongToDirection(vec2 latlong)
{
  float phi = latlong.x;
  float theta = latlong.y;
  float cosTheta = cos(theta);
  return vec3(cos(phi) * cosTheta, sin(theta), sin(phi) * cosTheta);
}


vec2 directionToLatLong(vec3 dir)
{
  float phi = atan(dir.z, dir.x);
  float theta = asin(dir.y);
  return vec2(phi, theta);
}


vec2 latLongToUV(vec2 latlong)
{
  return latlong * vec2(0.5, 1.0) * M_INV_PI + 0.5;
}


vec3 environment(vec3 dir, float lod)
{
  vec2 uv = latLongToUV(directionToLatLong(dir));
  vec3 t0 = textureLod(iChannel0, uv, int(floor(lod))).rgb;
  vec3 t1 = textureLod(iChannel0, uv, int(ceil (lod))).rgb;
  return mix(t0, t1, fract(lod));
}


mat4 perspective(in float fovy, in float aspect, in float zNear, in float zFar)
{
  float y = 1.0 / tan(fovy / 2.0);
  float zRange = 1.0 / (zNear - zFar);
  mat4 m = mat4(1.0);
  m[0][0] = y / aspect;
  m[1][1] = y;
  m[2][2] = (zFar + zNear) * zRange;
  m[2][3] = 2.0 * zFar * zNear * zRange;
  m[3][2] = -1.0;
  return m;
}


mat4 lookin(vec3 dir, vec3 up)
{
  vec3 f = normalize(dir);
  vec3 s = cross(f, normalize(up));
  vec3 u = cross(normalize(s), f);
  return transpose(mat4(vec4(s, 0.0),
                        vec4(u, 0.0),
                        vec4(-f, 0.0),
                        vec4(0.0, 0.0, 0.0, 1.0)));
}


mat4 rotateY(float angleInRadians)
{
  float c = cos(angleInRadians);
  float s = sin(angleInRadians);
  mat4 m = mat4(1.0);
  m[0][0] = c;
  m[0][2] = -s;
  m[2][0] = s;
  m[2][2] = c;
  return m;
}


vec3 unproject(in vec2 fragCoord, in mat4 modelViewProj)
{
  vec3 p = 2.0 * vec3(fragCoord, 1.0) / vec3(iResolution.xy, 1.0) - 1.0;
  p.y = -p.y;
  vec4 pdash = inverse(modelViewProj) * vec4(p, 1.0);
  return pdash.xyz / pdash.w;
} 


void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
  mat4 proj = perspective(45.0, iResolution.x / iResolution.y, 0.01, 100.0);
  mat4 modelview = rotateY(iGlobalTime);
  mat4 mvp = proj * modelview;

  vec3 forward = normalize(unproject(fragCoord, mvp));

  vec3 color = environment(forward, 0.0);

  // Gamma correct
  color = pow(color, vec3(1.0 / 2.2));

  fragColor = vec4(color, 1.0);
}
