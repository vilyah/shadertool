const float frameDuration = 1.0 / 60.0;
const float motionBlurWindow = 0.1; // seconds to motion blur over
const int motionBlurSamples = 20;  // number of motion blur samples
const float motionBlurDt = motionBlurWindow / float(motionBlurSamples - 1);

const float tileRotationSpeed = -1.67;


vec4 tileColor(vec2 uvT, vec2 numTiles, float t)
{
  return vec4(floor(uvT) / numTiles, 0.5, 1.0);
}


bool inTile(vec2 uvT)
{
  vec2 tmp = abs(uvT);
  return tmp.x <= 0.5 && tmp.y < 0.5;
}


mat2 rotation(float angle)
{
  float c = cos(angle);
  float s = sin(angle);
  return mat2(c, -s, s, c);
}


vec4 rotatedTile(vec2 uv, vec2 numTiles, float t)
{
  mat2 m = rotation(t * tileRotationSpeed - texture2D(iChannel0, uv).x * 0.2);
  
  vec2 uvT = uv * numTiles;
  vec2 lo = floor(uvT) - 0.5;
  vec2 hi = lo + 1.0;
  vec2 hl = vec2(hi.x, lo.y);
  vec2 lh = vec2(lo.x, hi.y);
  
  vec4 color = vec4(0.3);
  if (inTile(m * (uvT - lo))) {
    color = tileColor(m * (uvT - lo) + lo, numTiles, t);
  }
  else if (inTile(m * (uvT - hl))) {
    color = tileColor(m * (uvT - hl) + hl, numTiles, t);
  }
  else if (inTile(m * (uvT - lh))) {
    color = tileColor(m * (uvT - lh) + lh, numTiles, t);
  }
  else if (inTile(m * (uvT - hi))) {
    color = tileColor(m * (uvT - hi) + hi, numTiles, t);
  }
  
  return color;
}


void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
  float yScale = iResolution.y / iResolution.x;
  float size = (sin(iGlobalTime) + 1.0) * 32.0 + 32.0;
  vec2 tileSize = vec2(size, size * yScale);
  vec2 numTiles = iResolution.xy / tileSize;  
  
  vec4 color = vec4(0.0);
  float weightScale = 2.0 / float(motionBlurSamples * (motionBlurSamples + 1));
  for (int i = 0; i < motionBlurSamples; ++i) {
    float frame = iGlobalTime - motionBlurDt * float(i);
    float weight = float(motionBlurSamples - i) * weightScale;
    vec2 uv = rotation(frame) * (fragCoord.xy / iResolution.xy - 0.5) + 0.5;
    color += rotatedTile(uv, numTiles, frame) * weight;
  }

  fragColor = color;
}
