#define M_PI 3.1415926535897932384626433832795

const float epsilon = 1e-4;


float sdTorus(vec3 pos, vec2 t) {
  vec2 q = vec2(length(pos.xz) - t.x, pos.y);
  return length(q) - t.y;
}


float sceneDist(vec3 pos)
{
  float s = sin(iGlobalTime);
  float c = cos(iGlobalTime);
  mat3 rotX = mat3( 1.0, 0.0, 0.0,
                    0.0,   c,   s,
                    0.0,  -s,   c );
  mat3 rotY = mat3(   c, 0.0,   s,
                    0.0, 1.0, 0.0,
                     -s, 0.0,   c );
  vec3 tmpPos = rotY * rotX * pos;
  return sdTorus(tmpPos, vec2(0.8, 0.3));
}


vec3 sceneNormal(vec3 pos)
{
  vec3 dx = vec3(epsilon, 0.0, 0.0);
  vec3 dy = vec3(0.0, epsilon, 0.0);
  vec3 dz = vec3(0.0, 0.0, epsilon);
  return normalize(vec3( sceneDist(pos + dx) - sceneDist(pos - dx),
                         sceneDist(pos + dy) - sceneDist(pos - dy),
                         sceneDist(pos + dz) - sceneDist(pos - dz) ));
}


float intersect(vec3 pos, vec3 dir, float t, float maxT)
{
  while (t < maxT) {
    vec3 p = pos + dir * t;
    float d = abs(sceneDist(p));
    if (d < epsilon) {
      return t;
    }
    t += max(d, epsilon);
  }
  return 1e20;
}


void raymarch(vec3 pos, vec3 dir, float t, float maxT, out vec3 color)
{
  vec3 absorption = vec3(0.5);

  float oldT;
  while (t < maxT) {
    vec3 p = pos + dir * t;
    float 
    oldT = t;
  }
}


void mainImage(out vec4 fragColor, in vec2 fragCoord) {
  vec2 ndc = fragCoord.xy / iResolution.xy * 2.0 - 1.0;
  float w = atan(M_PI / 3.0);
  vec3 pos = vec3(0.0, 0.0, 5.0);
  vec3 dir = normalize(vec3(w * ndc.x, w * iResolution.y / iResolution.x * ndc.y, -1.0));
  
  float maxT = 200.0;
  float t = intersect(pos, dir, 0.0, maxT);
  if (t < maxT) {
    //vec3 color = vec3(0.0, 0.0, 0.0);
    //raymarch(pos, dir, t + epsilon, maxT, color);
    vec3 N = sceneNormal(pos + dir * t);
    vec3 L = normalize(vec3(1.0));
    float NdotL = dot(N, L);
    fragColor = vec4(0.33, 0.87, 0.33, 1.0) * NdotL;
  }
  else {
    fragColor = vec4(0.1);
  }
}
