#define M_PI     3.14159265358979
#define M_INV_PI 0.31830988618379


const uint numSamples = 256u;
const float lodScale = 4096.0;


vec2 uvToLatLong(vec2 uv)
{
  return (uv - 0.5) * vec2(2.0, 1.0) * M_PI;
}


vec3 latLongToDirection(vec2 latlong)
{
  float phi = latlong.x;
  float theta = latlong.y;
  float cosTheta = cos(theta);
  return vec3(cos(phi) * cosTheta, sin(theta), sin(phi) * cosTheta);
}


vec2 directionToLatLong(vec3 dir)
{
  float phi = atan(dir.z, dir.x);
  float theta = asin(dir.y);
  return vec2(phi, theta);
}


vec2 latLongToUV(vec2 latlong)
{
  return latlong * vec2(0.5, 1.0) * M_INV_PI + 0.5;
}


vec3 environment(vec3 dir, float lod)
{
  vec2 uv = latLongToUV(directionToLatLong(dir));
  vec3 t0 = textureLod(iChannel0, uv, int(floor(lod))).rgb;
  vec3 t1 = textureLod(iChannel0, uv, int(ceil (lod))).rgb;
  return mix(t0, t1, fract(lod));
}


float radicalInverse_VdC(uint bits)
{
  bits = (bits << 16u) | (bits >> 16u);
  bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
  bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
  bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
  bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
  return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}


vec2 hammersley(uint i, uint numSamples)
{
  return vec2(float(i) / float(numSamples), radicalInverse_VdC(i));
}


vec2 uniformSampleDisk(vec2 uv)
{
  float r = sqrt(uv.x);
  float theta = 2.0 * M_PI * uv.y;
  return vec2(r * cos(theta), r * sin(theta)) * 0.5 + 0.5;
}


vec3 cosineSampleHemisphere(vec2 uv)
{
  float r = sqrt(uv.x);
  float theta = 2.0 * M_PI * uv.y;
  return vec3(r * cos(theta), r * sin(theta), sqrt(max(0.0, 1.0 - uv.x)));
}


mat3 tangentToWorld(vec3 N)
{
  vec3 Tz = N;
  vec3 up = (Tz.y < 0.999) ? vec3(0.0, 1.0, 0.0) : vec3(1.0, 0.0, 0.0);
  vec3 Tx = cross(up, Tz);
  vec3 Ty = cross(Tx, Tz);
  return mat3(Tx, Ty, Tz);
}


vec3 prefilterEnvMap(float roughness, vec3 N)
{
  vec3 V = N;

  vec3 prefilteredColor = vec3(0.0);
  float totalWeight = 0.0;

  float a = roughness * roughness;
  a = a * a;

  mat3 ttw = tangentToWorld(N);
  float lod = log2(lodScale) * roughness;

  for (uint i = 0; i < numSamples; ++i) {
    vec2 Xi = hammersley(i, numSamples);
    Xi.x *= a; // We're treating Xi as polar coords, .x is the radius
    vec3 H = ttw * cosineSampleHemisphere(Xi);
    vec3 L = reflect(-V, H);

    float NdotL = dot(N, L);
    if (NdotL > 0.0) {
      prefilteredColor += environment(L, lod) * NdotL;
      totalWeight += NdotL;
    }
  }

  return prefilteredColor / totalWeight;
}


float tent(float x)
{
  return min(x, 1.0 - x) * 2.0;
}


void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
  vec2 uv = vec2(fragCoord.x, iResolution.y - fragCoord.y) / iResolution.xy;

  float roughness = tent(fract(iGlobalTime / 20.0));
  vec3 dir = normalize(latLongToDirection(uvToLatLong(uv)));
  vec3 color = prefilterEnvMap(roughness, dir);

  // Gamma correct
  color = pow(color, vec3(1.0 / 2.2));

  fragColor = vec4(color, 1.0);
}
