#define M_SQRT_2  1.4142135623730951
#define M_PI      3.141592653589793


//
// Helpers
//

float fmod(float x, float y)
{
  return fract(x / y) * y;
}


//
// CSG operations
//

float union(float d1, float d2)
{
  return min(d1, d2);
}


float difference(float d1, float d2)
{
  return max(d1, -d2);
}


float intersection(float d1, float d2)
{
  return max(d1, d2);
}


//
// Domain transformantion
//

vec2 repeat(vec2 pos, vec2 period)
{
  vec2 p = pos + period / 2.0;
  p.x = fmod(p.x, period.x);
  p.y = fmod(p.y, period.y);
  p -= period / 2.0;
  return p;
}


mat2 rotation(float angle)
{
  float s = sin(iGlobalTime);
  float c = cos(iGlobalTime);
  return mat2(c, -s, s, c);
}


//
// Shapes
//

float plane(vec2 pos, vec3 coeffs)
{
  return dot(pos, coeffs.xy) - coeffs.z;
}


float circle(vec2 pos, float size)
{
  return length(pos) - size / 2.0;
}


float square(vec2 pos, float size)
{
  return max(abs(pos.x), abs(pos.y)) - size / (2.0 * M_SQRT_2);
}


float triangle(vec2 pos, float size)
{
  float x = M_SQRT_2 / 2.0 * (pos.x - pos.y);
  float y = M_SQRT_2 / 2.0 * (pos.x + pos.y);
  return max(max(abs(x), abs(y)) - size / (2.0 * M_SQRT_2), pos.y);
}


float diamond(vec2 pos, float size)
{
  float x = M_SQRT_2 / 2.0 * (pos.x - pos.y);
  float y = M_SQRT_2 / 2.0 * (pos.x + pos.y);
  return max(abs(x), abs(y)) - size / (2.0 * M_SQRT_2);
}


float heart(vec2 pos, float size)
{
  float x = M_SQRT_2 / 2.0 * (pos.x - pos.y);
  float y = M_SQRT_2 / 2.0 * (pos.x + pos.y);
  float k = size / 3.5;
  return min( max(abs(x), abs(y)) - k,
         min( length(pos - M_SQRT_2 / 2.0 * vec2( k, -k)) - k,
              length(pos - M_SQRT_2 / 2.0 * vec2(-k, -k)) - k
         ));
}


float spade(vec2 pos, float size)
{
  // Head = heart flipped on y axis, scaled down by 0.85 & translated up slightly
  float k = size * 0.85 / 3.5;
  float x = M_SQRT_2 / 2.0 * (pos.x + pos.y) + 0.4 * k;
  float y = M_SQRT_2 / 2.0 * (pos.x - pos.y) - 0.4 * k;
  float h = min( max(abs(x), abs(y)) - k,
            min( length(pos - M_SQRT_2 / 2.0 * vec2( 1.0, 0.2) * k) - k,
                 length(pos - M_SQRT_2 / 2.0 * vec2(-1.0, 0.2) * k) - k
            ));

  // Tail = square and two circles
  float c = min( length(pos - vec2( 0.65, 0.125) * size) - size / 1.6,
                 length(pos - vec2(-0.65, 0.125) * size) - size / 1.6 );
  float p = square(pos - vec2(0.0, 0.3) * size, 0.6 * size);

  return min(h, max(-c, p));
}


float club(vec2 pos, float size)
{
  // Head = 3 circles
  float k = size * 0.5;
  float q0 = circle(pos + vec2(-0.4, -0.15) * k, k);
  float q1 = circle(pos + vec2( 0.4, -0.15) * k, k);
  float q2 = circle(pos + vec2( 0.0,  0.5) * k, k);
  float q = min(q0, min(q1, q2));
  
  // Tail = square and two circles
  float c = min( length(pos - vec2( 0.65, 0.125) * size) - size / 1.6,
                 length(pos - vec2(-0.65, 0.125) * size) - size / 1.6 );
  float p = square(pos - vec2(0.0, 0.3) * size, 0.6 * size);

  return min(q, max(-c, p));
}


//
// Scene
//

vec3 scene(vec2 pos)
{
  float scale = (1.0 + sin(iGlobalTime) * 0.5) * 0.2;
  
  mat2 r = rotation(iGlobalTime);
  pos = r * repeat(pos, vec2(scale * 3.1));

  vec3 color = vec3(1.0); // background
  float minD = 1e20;
  float d;

  d = min( heart  (pos + vec2(-0.5,  0.5) * scale, scale),
           diamond(pos + vec2( 0.5, -0.5) * scale, scale) );
  if (d < minD) {
    if (d <= 0.0) 
      color = vec3(1.0, 0.0, 0.0);
    minD = d;
  }

  d = min( spade(pos + vec2( 0.5,  0.5) * scale, scale),
           club (pos + vec2(-0.5, -0.5) * scale, scale) );
  if (d < minD) {
    if (d <= 0.0) 
      color = vec3(0.0);
    minD = d;
  }

  return color;
}


void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
  vec2 pos = fragCoord.xy / iResolution.xy * 2.0 - 1.0;
  pos.y *= iResolution.y / iResolution.x;

  vec3 color = scene(pos);
  fragColor = vec4(color, 1.0);
}
