#define M_PI 3.1415926535897932384626433832795

// Camera extrinsics
const vec3 cameraPos = vec3(0.0, 1.6, 3.0);
const vec3 cameraTarget = vec3(0.0, 0.5, 0.0);
const vec3 cameraUp = vec3(0.0, 1.0, 0.0);

// Camera intrinsics
const float imagePlaneDistance = 5.0;
const float fieldOfViewInRadians = 120.0 * M_PI / 180.0;
const float nearClip = 0.1;
const float farClip = 100.0;

// Raymarching parameters
const int maxViewSteps = 64;
const int maxLightSteps = 32;
const float epsilon = 1e-3;
const float minStep = 0.0001;

// Lighting parameters
const vec3 lightPos = vec3(10.0);

const vec4 primColor = vec4(0.67, 0.67, 0.67, 1.0);


// Signed distance to a sphere of radius r centred at the origin.
float sdSphere(vec3 pos, float r)
{
  return length(pos) - r;
}


// Signed distance to a plane described by the equation:
//   pos.x * plane.x + pos.y * plane.y + pos.z * plane.z + plane.w == 0;
// plane must be normalized.
float sdPlane(vec3 pos, vec4 plane)
{
  return dot(pos, plane.xyz) + plane.w;
}


// Signed distance to an axis-aligned box centred at the origin, with side
// lengths s.
float sdBox(vec3 pos, vec3 s)
{
  vec3 d = abs(pos) - s;
  return min(max(d.x, max(d.y, d.z)), 0.0) + length(max(d, 0.0));
}


// Signed distance to a cylinder centred at the origin. 
float sdCylinder(vec3 pos, vec3 c)
{
  return length(pos.xz - c.xy) - c.z;
}


// Signed distance to a line starting at the origin and ending at l, with thickness r
float sdLine(vec3 pos, vec3 l, float r)
{
  float t = clamp(dot(pos, l) / dot(l, l), 0.0, 1.0);
  return distance(pos, l * t) - r;
}


// Subtract distance field d2 from distance field d1.
float opSubtract(float d1, float d2)
{
  return max(-d1, d2);
}


// Return the intersection of two distance fields.
float opIntersection(float d1, float d2)
{
  return max(d1, d2);
}


// Return the union of two distance fields.
float opUnion(float d1, float d2)
{
  return min(d1, d2);
}


float sdSwirlyLine(vec3 pos, vec3 start, int segments, float segLength, float thickness)
{
  vec3 p0 = start;
  vec3 p1;
  int i = 0;
  float d = 1e20;
  float xWeight = 2.0;
  float zWeight = 2.3;
  while (i < segments) {
    p1 = normalize(vec3(sin(iGlobalTime * xWeight), 1.0, cos(iGlobalTime * zWeight))) * segLength;
    d = opUnion(d, sdLine(pos - p0, p1, thickness));
    p0 += p1;
    xWeight *= 1.3;
    zWeight *= 1.3;
    ++i;
  }
  return d;
}


// Get the signed distance to the nearest object in the scene
//
// The x component of the return value is the signed distance, the y component
// is the unsigned distance and the z component is the ID of the closest
// object.
vec3 map(vec3 pos)
{
  vec3 d;
  float tmp;

  // background
  vec3 dMin = vec3(200.0, 200.0, -1.0);

  // ground plane
  d = vec3(pos.y, abs(pos.y), 0.0);
  if (d.y < dMin.y) {
    dMin = d;
  }

  // lerping between union and difference of sphere & cube
  /*
  float d2 = sdSphere(pos - vec3(0.0, 1.0, 0.0), 0.5);
  float d1 = sdCylinder(pos - vec3(0.0, 1.0, 0.0), vec3(0.4, 0.4, 0.4));
  tmp = mix(opUnion(d1, d2), opSubtract(d1, d2), sin(iGlobalTime) / 2.0 + 0.5);
  d = vec3(tmp, abs(tmp), 1.0);
  if (d.y < dMin.y) {
    dMin = d;
  }
  */

  /*
  vec3 p0 = vec3(0.0, 0.0, 0.0);
  vec3 p1 = vec3(sin(iGlobalTime * 2.0), 1.0, cos(iGlobalTime * 2.0)) + p0;
  vec3 p2 = vec3(sin(iGlobalTime * 3.67), 2.0, cos(iGlobalTime * 3.79)) + p1;
  tmp = sdLine(pos - p0, p1, 0.02);
  tmp = opUnion(tmp, sdLine(pos - midPoint, endPoint, 0.02))
  tmp = opUnion(tmp, sdLine(pos - midPoint, endPoint, 0.02))
  d = vec3(tmp, abs(tmp), 1.0);
  if (d.y < dMin.y) {
    dMin = d;
  }
  */
  tmp = sdSwirlyLine(pos, vec3(0.0, 0.0, 0.0), 5, 0.5, 0.03);
  d = vec3(tmp, abs(tmp), 1.0);
  if (d.y < dMin.y) {
    dMin = d;
  }

  return dMin;
}


// Cast a ray into the scene and see what it hits.
//
// The xyz component of the return value is the position of the intersection;
// the w component is the object ID that we hit.
//
// If we don't hit anything, the return value will have the xyz components set
// to the ray direction and w component set to -1.0 (the ID of the background).
vec4 intersect(vec3 pos, vec3 dir, float minT, float maxT, int maxSteps)
{
  float t = minT;
  for (int i = 0; i < maxSteps; ++i) {
    vec3 p = pos + dir * t;
    vec3 d = map(p);
    if (d.y < epsilon) {
      return vec4(p, d.z);
    }
    t += max(d.y, 0.001);
    if (t >= maxT) {
      break;
    }
  }
  return vec4(0.0, 0.0, 0.0, -1.0);
}


// Get the normal vector at a particular point in the scene.
vec3 grad(vec3 pos)
{
  return normalize(vec3(
    map(pos + vec3(epsilon, 0.0, 0.0)).x - map(pos - vec3(epsilon, 0.0, 0.0)).x,
    map(pos + vec3(0.0, epsilon, 0.0)).x - map(pos - vec3(0.0, epsilon, 0.0)).x,
    map(pos + vec3(0.0, 0.0, epsilon)).x - map(pos - vec3(0.0, 0.0, epsilon)).x
  ));
}


/// Get the view ray for the fragment we're working on.
void view(out vec3 pos, out vec3 dir, in vec2 fragCoord)
{
  vec2 uv = (fragCoord.xy / iResolution.xy) * 2.0 - 1.0;
  vec3 cameraDir = normalize(cameraTarget - cameraPos);
  vec3 cameraRight = normalize(cross(cameraDir, cameraUp));
  float aspectRatio = iResolution.y / iResolution.x;
  float widthDistanceRatio = atan(fieldOfViewInRadians / 2.0);

  float imagePlaneHalfWidth = widthDistanceRatio * imagePlaneDistance;
  float imagePlaneHalfHeight = imagePlaneHalfWidth * aspectRatio;

  float nearClipHalfWidth = widthDistanceRatio * nearClip;
  float nearClipHalfHeight = nearClipHalfWidth * aspectRatio;

  vec3 viewRayTarget = cameraPos
                     + cameraDir * imagePlaneDistance
                     + cameraRight * uv.x * imagePlaneHalfWidth
                     + cameraUp * uv.y * imagePlaneHalfHeight;
  vec3 nearClipTarget = cameraPos 
                      + cameraDir * nearClip
                      + cameraRight * uv.x * nearClipHalfWidth
                      + cameraUp * uv.y * nearClipHalfHeight;

  pos = nearClipTarget;
  dir = normalize(viewRayTarget - nearClipTarget);
}



void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
  vec3 pos, dir;
  view(pos, dir, fragCoord);

  vec4 color = vec4(0.4, 0.44, 0.88, 1.0); // background

  vec4 hit = intersect(pos, dir, 0.0, 200.0, maxViewSteps);

  // If we didn't hit any objects, check if we hit the ground plane.
  if (hit.w < 0.0) {
    float t = (pos.y / -dir.y);
    if (t > 0.0 && t < 200.0) {
      hit = vec4(pos + dir * t, 0.0);
    }
  }

  if (hit.w >= 0.0) {
    // Shade the intersection point.
    vec3 N = grad(hit.xyz);
    vec3 L = normalize(lightPos - hit.xyz);
    float NdotL = max(dot(N, L), 0.0);

    vec4 lightHit = intersect(hit.xyz, L, 6.0 * epsilon, 20.0, maxLightSteps);
    float lightAmount = (lightHit.w >= 0.0) ? 0.0 : 1.0;

    color = vec4(primColor.rgb * NdotL * lightAmount, primColor.a);
  }

  fragColor = color;
}
