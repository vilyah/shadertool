#define M_PI 3.1415926535897932384626433832795

// Raymarching parameters
const int maxViewSteps = 128;
const int maxLightSteps = 128;
const float epsilon = 1e-4;

// Materials
struct Material {
  vec3 diffuse;
  vec3 specular;
  float specularPow;
};

// One entry per object ID.
const Material materials[4] = Material[](
  Material(vec3(0.76, 0.74, 0.88), vec3(0.0),  1.0), // background
  Material(vec3(0.48, 0.48, 0.48), vec3(0.6), 90.0), // ground plane
  Material(vec3(0.67, 0.45, 0.45), vec3(0.9), 10.0), // box - sphere
  Material(vec3(0.97, 0.95, 0.45), vec3(1.0), 40.0)  // inner sphere
);

// Hit record from raycasting.
struct Hit {
  vec3 pos;     // Location of the hit.
  int obj;      // ID of the object we hit.
  int numSteps; // Number of steps it took us to reach the hit point.
};


// ------- Signed distance functions -------

// Signed distance to a sphere of radius r centred at the origin.
float sdSphere(vec3 pos, float r)
{
  return length(pos) - r;
}


// Signed distance to a plane described by the equation:
//   pos.x * plane.x + pos.y * plane.y + pos.z * plane.z + plane.w == 0;
// plane must be normalized.
float sdPlane(vec3 pos, vec4 plane)
{
  return dot(pos, plane.xyz) + plane.w;
}


// Signed distance to an axis-aligned box centred at the origin, with side
// lengths s.
float sdBox(vec3 pos, vec3 s)
{
  vec3 d = abs(pos) - s;
  return min(max(d.x, max(d.y, d.z)), 0.0) + length(max(d, 0.0));
}


// Unsigned distance to a box with rounded edges.
float udRoundedBox(vec3 pos, vec3 s, float r)
{
  return length(max(abs(pos) - s, 0.0)) - r;
}


// Subtract distance field d2 from distance field d1.
float opSubtract(float d1, float d2)
{
  return max(d1, -d2);
}


vec3 domRepeat(vec3 pos, vec3 period)
{
  vec3 halfPeriod = period * 0.5;
  return mod(pos + halfPeriod, period) - halfPeriod;
}


// ------ The scene interface ------

// pos: position in world space.
// return value: x = signed distance, y = object ID
vec2 sceneDist(vec3 pos)
{
  float minSignedDist = 1e20;
  int objectID = 0;
  float signedDist;

  // ground plane
  signedDist = sdPlane(pos, vec4(0.0, 1.0, 0.0, 1.5));
  if (signedDist < minSignedDist) {
    minSignedDist = signedDist;
    objectID = 1;
  }

  // box - sphere
  float s = sin(iGlobalTime);
  float c = cos(iGlobalTime);
  mat3 rotX = mat3( 1.0, 0.0, 0.0,
                    0.0,   c,   s,
                    0.0,  -s,   c );
  mat3 rotY = mat3(   c, 0.0,   s,
                    0.0, 1.0, 0.0,
                     -s, 0.0,   c );
  vec3 tmpPos = rotY * rotX * pos;
  float d1 = udRoundedBox(tmpPos, vec3(0.4, 0.4, 0.4), 0.02);
  float d2 = sdSphere(tmpPos, 0.5);
  signedDist = opSubtract(d1, d2);
  if (signedDist < minSignedDist) {
    minSignedDist = signedDist;
    objectID = 2;
  }

  // Inner sphere
  signedDist = sdSphere(tmpPos, 0.2 + sin(iGlobalTime * 3.0) * 0.1);
  if (signedDist < minSignedDist) {
    minSignedDist = signedDist;
    objectID = 3;
  }

  return vec2(minSignedDist, float(objectID));
}


vec3 sceneNormal(Hit hit)
{
  if (hit.obj == 1) {
    return vec3(0.0, 1.0, 0.0);
  }
  vec3 dx = vec3(epsilon, 0.0, 0.0);
  vec3 dy = vec3(0.0, epsilon, 0.0);
  vec3 dz = vec3(0.0, 0.0, epsilon);
  return normalize(vec3( sceneDist(hit.pos + dx).x - sceneDist(hit.pos - dx).x,
                         sceneDist(hit.pos + dy).x - sceneDist(hit.pos - dy).x,
                         sceneDist(hit.pos + dz).x - sceneDist(hit.pos - dz).x ));
}


// ------- Raycasting -------

// Cast a ray into the scene and return the position & object ID of the first hit.
Hit intersect(vec3 pos, vec3 dir, float minT, float maxT, int maxSteps)
{
  float t = minT;
  Hit hit = Hit(dir, -1, 0);
  for (int i = 0; i < maxSteps; ++i) {
    vec3 p = pos + dir * t;
    vec2 d = sceneDist(p);
    float unsignedDist = abs(d.x);
    if (unsignedDist < epsilon) {
      hit.pos = p;
      hit.obj = int(d.y);
      break;
    }
    t += max(unsignedDist, epsilon);
    ++hit.numSteps;
    if (t >= maxT) {
      break;
    }
  }

  return hit;
}


// Get the view ray for the fragment we're working on.
void view(out vec3 pos, out vec3 dir, in vec2 fragCoord)
{
  // Camera extrinsics
  vec3 cameraPos = vec3(cos(iGlobalTime), 1.2 + sin(iGlobalTime) * 0.5, 3.0);
  vec3 cameraTarget = vec3(0.0, 0.0, 0.0);
  vec3 cameraUp = vec3(0.0, 1.0, 0.0);

  // Camera intrinsics
  float imagePlaneDistance = distance(cameraPos, cameraTarget);
  float fieldOfViewInRadians = 120.0 * M_PI / 180.0;

  vec2 uv = (fragCoord.xy / iResolution.xy) * 2.0 - 1.0;
  vec3 cameraDir = normalize(cameraTarget - cameraPos);
  vec3 cameraRight = normalize(cross(cameraDir, cameraUp));
  float aspectRatio = iResolution.y / iResolution.x;
  float widthDistanceRatio = atan(fieldOfViewInRadians / 2.0);

  float imagePlaneHalfWidth = widthDistanceRatio * distance(cameraPos, cameraTarget);
  float imagePlaneHalfHeight = imagePlaneHalfWidth * aspectRatio;

  vec3 viewRayTarget = cameraTarget
                     + cameraRight * uv.x * imagePlaneHalfWidth
                     + cameraUp * uv.y * imagePlaneHalfHeight;
  pos = cameraPos;
  dir = normalize(viewRayTarget - cameraPos);
}


vec3 light()
{
  return vec3(sin(iGlobalTime * 2), 5.0, cos(iGlobalTime * 2)) * 2.0;
}


void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
  vec3 pos, dir;
  view(pos, dir, fragCoord);

  // Keep marching until we reach a solid surface.
  Hit hit = intersect(pos, dir, 0.0, 200.0, maxViewSteps);
  Material mat = materials[hit.obj];

  vec3 color = mat.diffuse;
  bool showStepCount = false;
  if (!showStepCount) {
    if (hit.obj > 0) {
      // Shade the intersection point.
      vec3 N = sceneNormal(hit);
      vec3 L = normalize(light() - hit.pos);
      float NdotL = max(dot(N, L), 0.0);

      if (NdotL > 0.0) {
        Hit lightHit = intersect(hit.pos, L, 6.0 * epsilon, 200.0, maxLightSteps);
        float lightAmount = (lightHit.obj >= 0) ? 0.0 : 1.0;
        color = (mat.diffuse * NdotL + mat.specular * pow(NdotL, mat.specularPow)) * lightAmount;
      }
      else {
        color = vec3(0.0);
      }
    }
  }
  else {
    color = mix(vec3(0.0, 0.0, 1.0), vec3(1.0, 0.0, 0.0), float(hit.numSteps) / float(maxViewSteps - 1));
  }
  fragColor = vec4(color, 1.0);
}
