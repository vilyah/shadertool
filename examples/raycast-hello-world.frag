#define M_PI 3.1415926535897932384626433832795

// Camera extrinsics
const vec3 cameraPos = vec3(0.0, 1.8, 4.0);
const vec3 cameraTarget = vec3(0.0, 0.5, 0.0);
const vec3 cameraUp = vec3(0.0, 1.0, 0.0);

// Camera intrinsics
const float imagePlaneDistance = 5.0;
const float fieldOfViewInRadians = 120.0 * M_PI / 180.0;
const float nearClip = 0.1;
const float farClip = 100.0;

// Raymarching parameters
const int maxViewSteps = 64;
const int maxLightSteps = 16;
const float epsilon = 1e-3;

// Lighting parameters


struct Scene {
  vec4 sphere;

  vec4 backgroundColor;
  vec4 diffuse[2];
  vec4 specular[2];
  float bump[2];

  vec3 lightPos;
};


/// Get the view ray for the fragment we're working on.
void getViewRay(out vec3 pos, out vec3 dir, in vec2 fragCoord)
{
  vec2 uv = (fragCoord.xy / iResolution.xy) * 2.0 - 1.0;
  vec3 cameraDir = normalize(cameraTarget - cameraPos);
  vec3 cameraRight = normalize(cross(cameraDir, cameraUp));
  float aspectRatio = iResolution.y / iResolution.x;
  float widthDistanceRatio = atan(fieldOfViewInRadians / 2.0);

  float imagePlaneHalfWidth = widthDistanceRatio * imagePlaneDistance;
  float imagePlaneHalfHeight = imagePlaneHalfWidth * aspectRatio;

  float nearClipHalfWidth = widthDistanceRatio * nearClip;
  float nearClipHalfHeight = nearClipHalfWidth * aspectRatio;

  vec3 viewRayTarget = cameraPos
                     + cameraDir * imagePlaneDistance
                     + cameraRight * uv.x * imagePlaneHalfWidth
                     + cameraUp * uv.y * imagePlaneHalfHeight;
  vec3 nearClipTarget = cameraPos 
                      + cameraDir * nearClip
                      + cameraRight * uv.x * nearClipHalfWidth
                      + cameraUp * uv.y * nearClipHalfHeight;

  pos = nearClipTarget;
  dir = normalize(viewRayTarget - nearClipTarget);
}


// Animate the scene. Calaculates all the animations for time t and stores them
// in the scene.
void update(float t, out Scene scene)
{
  scene.sphere.xyz = vec3(0.0, 1.5, 0.0) + vec3( sin(t * 2.1), cos(t * 1.7), 0.0 );
  scene.sphere.w = 0.5;

  scene.backgroundColor = vec4(0.30, 0.35, 0.88, 1.0);
  scene.diffuse[0] = vec4(0.50, 0.50, 0.50, 1.0); // ground plane colour
  scene.diffuse[1] = vec4(0.70, 0.00, 0.00, 1.0); // sphere colour
  scene.specular[0] = vec4(0.6);
  scene.specular[1] = vec4(1.0);
  scene.bump[0] = 0.0;
  scene.bump[1] = 0.6;

  scene.lightPos = vec3(10.0 * cos(t), 10.0, 10.0 * sin(t));
}


// Get the distance to the nearest object in the scene
//
// The x component of the return value is the signed distance, the y component
// is the unsigned distance and the z component is the ID of the closest
// object.
vec3 map(Scene scene, vec3 pos)
{
  vec3 d;
  float tmp;

  // background
  vec3 dMin = vec3(200.0, 200.0, -1.0);

  // ground plane
  d = vec3(pos.y, abs(pos.y), 0.0);
  if (d.y < dMin.y) {
    dMin = d;
  }

  // sphere 1
  tmp = distance(scene.sphere.xyz, pos) - scene.sphere.w;
  d = vec3(tmp, abs(tmp), 1.0);
  if (d.y < dMin.y) {
    dMin = d;
  }

  // sphere 2
  tmp = distance(scene.sphere.xyz, pos) - scene.sphere.w;
  d = vec3(tmp, abs(tmp), 2.0);
  if (d.y < dMin.y) {
    dMin = d;
  }

  return dMin;
}


vec3 mapDir(Scene scene, vec3 pos, vec3 dir)
{
  vec3 d;
  float tmp;

  // background
  vec3 dMin = vec3(200.0, 200.0, -1.0);

  // ground plane
  tmp = sign(pos.y) * length(dir * (pos.y / -dir.y));
  d = vec3(tmp, abs(tmp), 0.0);
  if (d.y < dMin.y) {
    dMin = d;
  }

  // sphere
  if (dot(normalize(scene.sphere.xyz - pos), dir) >= 0) {
    tmp = distance(scene.sphere.xyz, pos) - scene.sphere.w;
    d = vec3(tmp, abs(tmp), 1.0);
    if (d.y < dMin.y) {
      dMin = d;
    }
  }

  return dMin;
}


// Cast a ray into the scene and see what it hits.
//
// The xyz component of the return value is the position of the intersection;
// the w component is the object ID that we hit.
//
// If we don't hit anything, the return value will have the xyz components set
// to the ray direction and w component set to -1.0 (the ID of the background).
vec4 intersect(Scene scene, vec3 pos, vec3 dir, float minT, float maxT, int maxSteps)
{
  float t = minT;
  for (int i = 0; i < maxSteps; ++i) {
    vec3 p = pos + dir * t;
    vec3 d = mapDir(scene, p, dir);
    if (d.y < epsilon) {
      return vec4(p, d.z);
    }
    t += max(d.y, 0.001);
    if (t >= maxT) {
      break;
    }
  }
  return vec4(0.0, 0.0, 0.0, -1.0);
}


// Get the normal vector at a particular point in the scene.
vec3 grad(Scene scene, vec3 pos)
{
  return normalize(vec3(
    map(scene, pos + vec3(epsilon, 0.0, 0.0)).x - map(scene, pos - vec3(epsilon, 0.0, 0.0)).x,
    map(scene, pos + vec3(0.0, epsilon, 0.0)).x - map(scene, pos - vec3(0.0, epsilon, 0.0)).x,
    map(scene, pos + vec3(0.0, 0.0, epsilon)).x - map(scene, pos - vec3(0.0, 0.0, epsilon)).x
  ));
}


// Given a point on the surface of a unit sphere centred at the origin, return
// the corresponding UV coordinates.
vec2 sphereUV(vec3 pos)
{
  return vec2( 0.5 + atan(pos.z, pos.x) / (2 * M_PI),
               0.5 - asin(pos.y) / M_PI );
}


vec3 normal(Scene scene, vec4 hit)
{
  return normalize(grad(scene, hit.xyz));
}


void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
  Scene scene;
  update(iGlobalTime, scene);

  vec3 pos, dir;
  getViewRay(pos, dir, fragCoord);

  vec4 color = scene.backgroundColor;

  vec4 hit = intersect(scene, pos, dir, 0.0, 200.0, maxViewSteps);
  int id = int(hit.w);
  if (id >= 0) {
    vec3 bumpPos = (id == 1) ? (hit.xyz - scene.sphere.xyz) : hit.xyz;
    // Shade the intersection point.
    vec3 N = normal(scene, hit);
    vec3 L = normalize(scene.lightPos - hit.xyz);
    float NdotL = max(dot(N, L), 0.0);

    vec4 lightHit = intersect(scene, hit.xyz, L, 3.0 * epsilon, 20.0, maxLightSteps);
    float lightAmount = (int(lightHit.w) >= 0) ? 0.0 : 1.0;
    
    vec4 diffuse = vec4(scene.diffuse[id].rgb * NdotL * lightAmount, scene.diffuse[id].a);
    vec4 specular = vec4(scene.specular[id].rgb * pow(NdotL, 20.0) * lightAmount, scene.specular[id].a);

    color = diffuse + specular;
  }

  fragColor = color;
}

