precision highp float;

float vignette(vec2 p)
{
  float d = 0.5 - length(p - 0.5);
  float r = 0.25;
  return smoothstep(0.0, r, d + r);
}


void mainImage(out vec4 fragColor, in vec2 fragCoord) {
  vec2 uv = fragCoord.xy / iResolution.xy;
  float x = (uv.x + iGlobalTime) * 0.27;
  vec4 c = pow(vec4(0.3, 0.3, 0.9, 1.0), vec4(uv.y));
  float h = 0.7 + sin(x) * 0.1 + sin(x * 21.054 + 3.0) * 0.03 + cos(x * 49.76 + 1.3) * 0.01;
  if (uv.y < h)
    c -= vec4(0.5);
  h = 0.3 + fract(sin(x * 12.98) * 43758.54) * 0.05;
  if (uv.y < h)
    c -= vec4(0.3);
  fragColor = mix(vec4(0.0, 0.2, 0.3, 1.0), c, vignette(uv));
}
