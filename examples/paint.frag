const float kPenRadius = 10.0;
const vec4 kPenColor = vec4(0.25, 0.1, 0.1, 1.0);

void mainImage(out vec4 fragColor, out vec4 feedbackOut, in vec2 fragCoord)
{
  vec2 uv = fragCoord / iResolution.xy;
  vec4 color = texture(iChannelFeedback, uv);

  vec2 mouseCoord = vec2(iMouse.x, iResolution.y - iMouse.y);
  if (iMouse.xy != iMouse.zw && distance(fragCoord, mouseCoord) < kPenRadius) {
    color += kPenColor;
  }

  fragColor = color;
  feedbackOut = color;
}