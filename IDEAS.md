Cube maps
---------

Allow user to specify a cube maps for any of the usual input channels,
instead of just a simple 2D texture.


Multi-shader projects
---------------------

Extend the project format to allow running a sequence of shaders each frame,
instead of just one.


Geometry support
----------------

Add a shader type which takes geometry as input. Geometry can be a hardcoded
shape such as a sphere, cylinder or cube; or it can be a user-specified model
in OBJ format.

