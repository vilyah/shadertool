TEMPLATE = app
CONFIG += console
#CONFIG -= app_bundle
#CONFIG -= qt

SOURCES += src/main.cpp \
    src/gl_44.c

HEADERS += \
    src/gl_44.h \
    src/stb_image.h \
    src/stb_truetype.h

linux {
  LIBS += -lglfw3 -lrt -lGL -lXrandr -lXxf86vm -lXi -lX11
}

win32 {
  INCLUDEPATH += D:\Code\3rdparty\glfw-3.0.4.bin.WIN64\include
  debug {
    LIBS += D:\Code\3rdparty\glfw-3.0.4.bin.WIN64\lib-msvc120\glfw3.lib
    LIBS += winmm.lib opengl32.lib glu32.lib
  }
}

OTHER_FILES += \
    README.md \
    examples/clouds.frag \
    examples/elevated.frag \
    examples/mike.frag \
    examples/seascape.frag \
    examples/spinningtiles.frag \
    examples/volcanic.frag \
    textures/tex00.jpg \
    textures/tex01.jpg \
    textures/tex02.jpg \
    textures/tex03.jpg \
    textures/tex04.jpg \
    textures/tex05.jpg \
    textures/tex06.jpg \
    textures/tex07.jpg \
    textures/tex08.jpg \
    textures/tex09.jpg \
    textures/tex10.png \
    textures/tex11.png \
    textures/tex12.png \
    textures/tex14.png \
    textures/tex15.png \
    textures/tex16.png \
    fonts/Prototype.ttf \
    fonts/Prototype.txt \
    examples/clouds.stool \
    examples/elevated.stool \
    examples/volcanic.stool \
    examples/spinningtiles.stool \
    examples/raycast-hello-world.frag \
    examples/mysterymountains.frag \
    examples/useful-functions.frag \
    examples/cards.frag \
    examples/feedback-hello-world.frag \
    examples/landscape.frag \
    examples/landscape-packed.frag \
    examples/primitives.frag \
    examples/raycast.frag \
    examples/smoke.frag \
    examples/training.frag \
    examples/training-packed.frag \
    examples/volume-raymarch.frag \
    examples/volumes.frag
