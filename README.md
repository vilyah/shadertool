ShaderTool
==========

A tool for experimenting with fragment shaders, inspired by [ShaderToy](https://www.shadertoy.com/).


Requirements
---

- Currently: an OpenGL 4.4 capable graphics card and drivers.
- [GLFW 3.x](http://www.glfw.org/) (tested with 3.0, should hopefully work with 3.1 as well).


Compiling
---

Use QtCreator:

- Open ShaderTool.pro as an existing project
- Hit Ctrl+B


Usage
---

To run:

    ShaderTool [options] [<file>]

where <file> can be a shader or a project (.stp) file. For more info about the available options, run

    ShaderTool --help


While running:

- Click and drag to interact with the shader (if the shader supports it).
- Hit <escape> to quit.


Writing Shaders
---------------

You provide a fragment shader containing a function with either of these two
signatures:

    void mainImage(out vec4 fragColor, in vec2 fragCoord);
    void mainImage(out vec4 fragColor, out vec4 feedbackOut, in vec2 fragCoord);

The first version of the mainImage function is for shaders which calculate a
colour entirely from the current frame parameters. It's compatible with
ShaderToy. The fragment shader should write the final output colour to the
fragColor variable.

The second version of the mainImage function adds an additional output buffer
which persists from frame to frame. You can read the previous frame's output
in the current frame. This allows implementing a variety of effects that
simply aren't possible with ShaderToy, but gives up compatibility with it as a
result.

You shouldn't include the #version or any #extension directives. You shouldn't
declare any unfiforms or in/out variables either. There are a number of
predefined uniforms that you can make use of in your
shader:

    uniform vec3      iResolution;           // viewport resolution (in pixels)
    uniform float     iGlobalTime;           // shader playback time (in seconds)
    uniform float     iChannelTime[4];       // channel playback time (in seconds)
    uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
    uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
    uniform samplerXX iChannel0..3;          // input channel. XX = 2D/Cube
    uniform sampler2D iChannelFeedback;      // feedback from the previous frame.
    uniform vec4      iDate;                 // (year, month, day, time in seconds)
    uniform float     iSampleRate;           // sound sample rate (i.e., 44100)

At this time, only the following inputs work:

- iResolution
- iGlobalTime
- iMouse
- iChannel0..3
- iChannelFeedback
- iChannelResolution

The following inputs provide values which are usable but not necessarily
correct:

- iChannelTime
- iDate
- iSampleRate
