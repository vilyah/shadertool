#include "gl_44.h"
#include <GLFW/glfw3.h>

#include <cstdarg>
#include <cstdio>
#include <string>

#ifdef WIN32
#include <windows.h>
#include <mmsystem.h>
#endif

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"


#ifdef WIN32
#define VH_PUBLIC __declspec(dllexport)
#else
#define VH_PUBLIC __attribute__ ((visibility ("default")))
#endif


// If we're running on a laptop with Optimus, this will force the program to
// run on the discrete Nvidia GPU rather than the integrated Intel one. The
// Intel GPU doesn't support OpenGL 4.4, so window creation fails if we don't
// do this.
//
// I found this info via StackOverflow:
// http://stackoverflow.com/questions/10535950/forcing-nvidia-gpu-programmatically-in-optimus-laptops
extern "C" {
  VH_PUBLIC unsigned long NvOptimusEnablement = 0x00000001;
}


namespace vh {

  //
  // Constants related to FPS counting
  //

  // This has to be an even number.
  static const int kMaxFrameTimes = 20;


  //
  // Types
  //

  /// The result of parsing the command line, indicates whether the program
  /// should continue, exit cleanly (e.g. if it encountered --help or --version
  /// options), or exit with an error (if the command line was invalid for some
  /// reason).
  enum CommandLineResult {
    eContinue,  //!< Everything ok, program should continue.
    eCleanExit, //!< Everything ok, program should exit without an error.
    eErrorExit  //!< Something was wrong, program should exit with an error.
  };


  /// Application-specific variables and settings.
  struct App {
    bool reload;
    bool shadertoyCompatibility;
    bool displayOverlay;  //!< Whether to show the time counter & other bits of UI text.

    // Font shader handles
    GLuint fontVertexShader;
    GLuint fontFragmentShader;
    GLuint fontProgram;

    // Font buffer handles
    GLuint fontVAO;
    GLuint fontBuf;

    // Font texture handles
    GLuint fontTex; //!< Texture holding the baked-out font.

    // Font shader uniform variable indexes
    GLint iResolution;
    GLint iFontTex;
    GLint iFontColor;

    // Font data
    stbtt_bakedchar bakedChars[96]; // ASCII 32..126 is 95 glyphs.

    App() :
      reload(true),
      shadertoyCompatibility(false),
      displayOverlay(true),

      fontVertexShader(0),
      fontFragmentShader(0),
      fontProgram(0),

      fontVAO(0),
      fontBuf(0),

      fontTex(0),

      iResolution(-1),
      iFontTex(-1),
      iFontColor(-1)
    {}
  };


  struct Shader {
    // Shader handles.
    GLuint vertexShader;
    GLuint fragmentShader;
    GLuint program;

    // Uniform variable indexes.
    GLint iResolution;
    GLint iGlobalTime;
    GLint iChannelTime;
    GLint iChannelResolution;
    GLint iMouse;
    GLint iChannel0;
    GLint iChannel1;
    GLint iChannel2;
    GLint iChannel3;
    GLint iChannelFeedback;
    GLint iDate;
    GLint iSampleRate;

    Shader() :
      vertexShader(0),
      fragmentShader(0),
      program(0),

      iResolution(-1),
      iGlobalTime(-1),
      iChannelTime(-1),
      iChannelResolution(-1),
      iMouse(-1),
      iChannel0(-1),
      iChannel1(-1),
      iChannel2(-1),
      iChannel3(-1),
      iChannelFeedback(-1),
      iDate(-1),
      iSampleRate(-1)
    {}
  };


  /// Project-specific variables and settings.
  struct Project {
    std::string name;
    std::string authorName;

    std::string shaderFilename;
    std::string channel0Filename;
    std::string channel1Filename;
    std::string channel2Filename;
    std::string channel3Filename;

    double globalTimeStart;
    double globalTimeStop;
    double globalTimeOffset; // Amount to add to the current global time due to pressing the skip forward/back buttons.
    bool paused;
    bool wasPaused;

    double frameTimes[kMaxFrameTimes];
    int currFrameTime;
    double frameTimesSum;

    // Values for the shader uniforms
    float resolution[3];            // Framebuffer resolution.
    float globalTime;               // The current playback time, in seconds since the start of playback.
    float channelTime[4];           // For animated textures.
    float channelResolution[4 * 3]; // Resolution for each of the textures.
    float mouseX;
    float mouseY;
    float clickX;
    float clickY;
    float year;
    float month;
    float day;
    float timeInSecs;
    float audioSampleRate;

    // Shaders
    Shader userShader;

    // Buffer handles.
    GLuint vao;
    GLuint vertexBuf;

    // Texture handles.
    GLuint texChannel0;
    GLuint texChannel1;
    GLuint texChannel2;
    GLuint texChannel3;
    GLuint outputTex;
    GLuint feedbackTex[2];

    // Framebuffer handles
    GLuint fbo;

    // Constructor which initialises everything to empty.
    Project() :
      name(""),
      authorName(""),

      shaderFilename(""),
      channel0Filename(""),
      channel1Filename(""),
      channel2Filename(""),
      channel3Filename(""),

      globalTimeStart(0.0),
      globalTimeStop(0.0),
      globalTimeOffset(0.0),
      paused(false),
      wasPaused(false),

      currFrameTime(0),
      frameTimesSum(0.0),

      globalTime(0.0),
      mouseX(0.0),
      mouseY(0.0),
      clickX(0.0),
      clickY(0.0),
      year(2015.0f),
      month(2.0f),
      day(11.0f),
      timeInSecs(0.0f),
      audioSampleRate(44100.0f),

      userShader(),

      texChannel0(0),
      texChannel1(0),
      texChannel2(0),
      texChannel3(0),
      outputTex(0)
    {
      for (int i = 0; i < kMaxFrameTimes; ++i)
        frameTimes[i] = 0.0;
      for (int i = 0; i < 3; ++i)
        resolution[i] = 0.0f;
      for (int i = 0; i < 4; ++i)
        channelTime[i] = 0.0f;
      for (int i = 0; i < 12; ++i)
        channelResolution[i] = 0.0f;
      for (int i = 0; i < 2; ++i)
        feedbackTex[i] = 0;
    }
  };


  //
  // Constants
  //

  static const char* kVertexShader =
      "#version 440\n"
      "\n"
      "in vec2 pos;\n"
      "\n"
      "void main() {\n"
      "  gl_Position = vec4(pos, 0.0, 1.0);\n"
      "}\n"
      "";

  static const char* kFragShaderVersion =
      "#version 440\n";

  static const char* kFragShaderInputs =
      "uniform vec3      iResolution;           // viewport resolution (in pixels)\n"
      "uniform float     iGlobalTime;           // shader playback time (in seconds)\n"
      "uniform float     iChannelTime[4];       // channel playback time (in seconds)\n"
      "uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)\n"
      "uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click\n"
      "uniform sampler2D iChannel0;             // input channel. XX = 2D/Cube\n"
      "uniform sampler2D iChannel1;             // input channel. XX = 2D/Cube\n"
      "uniform sampler2D iChannel2;             // input channel. XX = 2D/Cube\n"
      "uniform sampler2D iChannel3;             // input channel. XX = 2D/Cube\n"
      "uniform sampler2D iChannelFeedback;      // feedback value computed by the shader on the previous frame.\n"
      "uniform vec4      iDate;                 // (year, month, day, time in seconds)\n"
      "uniform float     iSampleRate;           // sound sample rate (i.e., 44100)\n"
      "";

  static const char* kFragShaderPrefix =
      "\n"
      "layout(location=0) out vec4 color;\n"
      "layout(location=1) out vec4 feedback;\n"
      "\n"
      "";

  static const char* kFragShaderBody =
      "void mainImage(out vec4 fragColor, out vec4 feedbackOut, in vec2 fragCoord) {\n"
      "  vec2 uv = fragCoord / iResolution.xy;\n"
      "  vec4 feedbackIn = texture(iChannelFeedback, uv);\n"
      "  fragColor = vec4(uv, feedbackIn.x, 1.0);\n"
      "  feedbackOut = vec4(sin(iGlobalTime) * 0.5 + 0.5, 0.0, 0.0, 0.0);\n"
      "}\n"
      "";

  static const char* kFragShaderSuffixNoFeedback =
      "\n"
      "void main() {\n"
      "  mainImage(color, gl_FragCoord.xy);\n"
      "}\n"
      "";

  static const char* kFragShaderSuffixFeedbackOn =
      "\n"
      "void main() {\n"
      "  mainImage(color, feedback, gl_FragCoord.xy);\n"
      "}\n"
      "";

  static const char* kFontVertexShader =
      "#version 440\n"
      "\n"
      "uniform vec2 iResolution;\n"
      "\n"
      "in vec2 pos;\n"
      "in vec2 uv;\n"
      "\n"
      "out vec2 fragUV;\n"
      "\n"
      "void main() {\n"
      "  float aspectRatio = iResolution.x / iResolution.y;\n"
      "  vec2 localPos = pos / iResolution * vec2(2.0, -2.0 / aspectRatio) - vec2(1.0);\n"
      "  gl_Position = vec4(localPos, 0.0, 1.0);\n"
      "  fragUV = uv;\n"
      "}\n"
      "";

  static const char* kFontFragmentShader =
      "#version 440\n"
      "\n"
      "uniform sampler2D iFontTex;\n"
      "uniform vec4 iFontColor;\n"
      "\n"
      "in vec2 fragUV;\n"
      "\n"
      "out vec4 color;\n"
      "\n"
      "void main() {\n"
      "  color = iFontColor * texture(iFontTex, fragUV).x;\n"
      "}\n"
      "";

  static const float kQuadVerts[] = {
    -1.0f, -1.0f,
    -1.0f,  1.0f,
     1.0f, -1.0f,
     1.0f,  1.0f
  };

  static const char kUnixDirSep = '/';
  static const char kWindowsDirSep = '\\';
#ifdef WIN32
  static const char kNativeDirSep = kWindowsDirSep;
#else
  static const char kNativeDirSep = kUnixDirSep;
#endif

  // Constants related to text rendering.
  static const int kMaxChars = 4096;
  static const int kFontTexWidth = 512;
  static const int kFontTexHeight = 512;


  //
  // Globals
  //

  static App gApp;


  //
  // Logging
  //

  static void err(const char* fmt, ...)
  {
    va_list args;
    va_start(args, fmt);
    fprintf(stderr, "ERROR: ");
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    va_end(args);
  }


  static void warn(const char* fmt, ...)
  {
    va_list args;
    va_start(args, fmt);
    fprintf(stderr, "Warning: ");
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    va_end(args);
  }


  static void info(const char* fmt, ...)
  {
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    va_end(args);
  }


  static void debug(const char* fmt, ...)
  {
#ifndef NDEBUG
    va_list args;
    va_start(args, fmt);
    fprintf(stderr, "DEBUG: ");
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    va_end(args);
#endif
  }


  //
  // Utility functions
  //

  double tick()
  {
#if defined(_WIN32)
    return double(timeGetTime()) / 1000.0;
#else
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);
    double now_ms = double(now.tv_sec) * 1000.0 + double(now.tv_nsec) / 1000000.0;
    return now_ms / 1000.0;
#endif
  }


  std::string trim(const std::string& s)
  {
    if (s.empty()) {
      return "";
    }

    size_t i = 0;
    size_t j = s.size();
    while (i < j && isspace(s[i])) {
      ++i;
    }
    while (j > i && isspace(s[j - 1])) {
      --j;
    }

    return s.substr(i, j - i);
  }


  char* loadFile(const std::string& filename)
  {
    FILE* f = fopen(filename.c_str(), "rb");
    if (!f) {
      err("couldn't open file %s", filename.c_str());
      return NULL;
    }

    fseek(f, 0, SEEK_END);
    size_t numBytes = ftell(f);
    fseek(f, 0, SEEK_SET);

    char* buf = new char[numBytes + 1];
    size_t numRead = fread(buf, 1, numBytes, f);
    buf[numBytes] = '\0';
    fclose(f);

    if (numRead != numBytes) {
      err("couldn't read file %s", filename.c_str());
      delete[] buf;
      return NULL;
    }

    return buf;
  }


  std::string fileExtension(const std::string& filename)
  {
    size_t n = filename.rfind('.');
    if (n == std::string::npos || n == 0) {
      return "";
    }
    else {
      return filename.substr(n + 1);
    }
  }


  std::string dirname(const std::string& filename)
  {
    const char sepChars[] = { kUnixDirSep, kWindowsDirSep, 0 };
    size_t n = filename.find_last_of(sepChars);
    if (n == std::string::npos) {
      return "";
    }
    else {
      return filename.substr(0, n);
    }
  }


  bool isAbsolutePath(const std::string& path)
  {
    if (path.empty()) {
      return false;
    }

    // Allow windows or unix style separators. May have to tighten this up
    // later on, but for now let's be permissive.
    if (path[0] == kUnixDirSep || path[0] == kWindowsDirSep) {
      return true;
    }

    // Check for a windows drive-letter, e.g. 'c:'
    size_t pos = path.find(':');
    if (pos != std::string::npos && pos != 0) {
      return true;
    }

    return false;
  }


  std::string relativeToDir(const std::string dirPath, const std::string& relPath)
  {
    if (isAbsolutePath(relPath) || dirPath.empty()) {
      return relPath;
    }
    return dirPath + kNativeDirSep + relPath;
  }


  std::string relativeToFile(const std::string& filePath, const std::string& relPath)
  {
    return relativeToDir(dirname(filePath), relPath);
  }


  //
  // GLFW callbacks
  //

  static void errorHandlerGLFW(int error, const char* description)
  {
    err("GLFW error %d: %s", error, description);
  }


  /// This function handles single key press or key release events. For
  /// repeating keys (e.g. the arrow keys) we poll them in the update()
  /// function instead.
  static void keyHandlerGLFW(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mods*/)
  {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
      glfwSetWindowShouldClose(window, GL_TRUE);
    }

    Project* project = reinterpret_cast<Project*>(glfwGetWindowUserPointer(window));
    if (project == NULL) {
      return;
    }

    if (action == GLFW_PRESS) {
      if (key == GLFW_KEY_SPACE) {
        project->paused = !project->paused;
      }
      else if (key == GLFW_KEY_HOME) {
        double now = tick();
        project->globalTimeStart = now;
        project->globalTimeStop = now;
        project->globalTimeOffset = 0.0;
      }
      else if (key == GLFW_KEY_TAB) {
        gApp.displayOverlay = !gApp.displayOverlay;
      }
    }

  }


  static void resizeHandlerGLFW(GLFWwindow* /*window*/, int /*width*/, int /*height*/)
  {
    // Anything to do here?
  }


  //
  // OpenGL callbacks
  //

  static void errorHandlerGL(GLenum /*source*/,
                             GLenum type,
                             GLuint /*id*/,
                             GLenum severity,
                             GLsizei /*length*/,
                             const GLchar* message,
                             const void* /*userParam*/)
  {
    const char* typeStr = NULL;
    switch (type) {
    case GL_DEBUG_TYPE_ERROR:
      typeStr = "error";
      break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      typeStr = "deprecated behaviour";
      break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      typeStr = "undefined behaviour";
      break;
    case GL_DEBUG_TYPE_PORTABILITY:
      typeStr = "portability issue";
      break;
    case GL_DEBUG_TYPE_PERFORMANCE:
      typeStr = "performance issue";
      break;
    case GL_DEBUG_TYPE_MARKER:
      typeStr = "marker";
      break;
    default:
      typeStr = "notification";
      break;
    }

    const char* severityStr = NULL;
    switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:
      severityStr = "high severity";
      break;
    case GL_DEBUG_SEVERITY_MEDIUM:
      severityStr = "medium severity";
      break;
    case GL_DEBUG_SEVERITY_LOW:
      severityStr = "low severity";
      break;
    default:
      severityStr = "informational";
      break;
    }

    warn("OpenGL %s, %s: %s", typeStr, severityStr, message);
  }


  //
  // Project loading
  //

  const char* field(const char* fieldName, const char* line)
  {
    if (line == NULL) {
      return NULL;
    }

    // Make sure that the line starts with the fieldName, followed by a colon.
    int i = 0;
    while (fieldName[i] != 0 && line[i] != 0 && fieldName[i] == line[i]) {
      ++i;
    }
    if (fieldName[i] != 0 || line[i] != ':') {
      return false;
    }
    ++i;

    // Allow any number of spaces following the colon.
    while (isspace(line[i])) {
      ++i;
    }

    // Return a pointer to the start of the field value.
    return line + i;
  }


  bool loadProject(const std::string& filename, Project& project)
  {
    FILE* f = fopen(filename.c_str(), "r");
    if (!f) {
      err("unable to open profile file '%s'", filename.c_str());
      return false;
    }

    const size_t maxLineLength = 32 * 1024;
    char* buf = new char[maxLineLength];
    const char* value;

    // First line must be the identifier for the file format.
    value = fgets(buf, maxLineLength, f);
    if (value == NULL || trim(value) != "ShaderTool Project") {
      err("file '%s' doesn't appear to be a ShaderTool project", filename.c_str());
      goto failed;
    }

    // Next line must be the project name field.
    value = field("ProjectName", fgets(buf, maxLineLength, f));
    if (value == NULL) {
      err("project '%s' is missing the ProjectName field", filename.c_str());
      goto failed;
    }
    project.name = trim(value);

    // Next line must be the AuthorName field.
    value = field("AuthorName", fgets(buf, maxLineLength, f));
    if (value == NULL) {
      err("project '%s' is missing the ProjectName field", filename.c_str());
      goto failed;
    }
    project.authorName = trim(value);

    // Next line must be the shader filename field.
    value = field("Shader", fgets(buf, maxLineLength, f));
    if (value == NULL) {
      err("project '%s' is missing the Shader field", filename.c_str());
      goto failed;
    }
    project.shaderFilename = relativeToFile(filename, trim(value));

    // Next line must be the channel 0 filename field.
    value = field("Channel0", fgets(buf, maxLineLength, f));
    if (value == NULL) {
      err("project '%s' is missing the Channel0 field", filename.c_str());
      goto failed;
    }
    if (*value == 0) {
      project.channel0Filename = "";
    }
    else {
      project.channel0Filename = relativeToFile(filename, trim(value));
    }

    // Next line must be the channel 1 filename field.
    value = field("Channel1", fgets(buf, maxLineLength, f));
    if (value == NULL) {
      err("project '%s' is missing the Channel1 field", filename.c_str());
      goto failed;
    }
    if (*value == 0) {
      project.channel1Filename = "";
    }
    else {
      project.channel1Filename = relativeToFile(filename, trim(value));
    }

    // Next line must be the channel 2 filename field.
    value = field("Channel2", fgets(buf, maxLineLength, f));
    if (value == NULL) {
      err("project '%s' is missing the Channel2 field", filename.c_str());
      goto failed;
    }
    if (*value == 0) {
      project.channel2Filename = "";
    }
    else {
      project.channel2Filename = relativeToFile(filename, trim(value));
    }

    // Next line must be the channel 3 filename field.
    value = field("Channel3", fgets(buf, maxLineLength, f));
    if (value == NULL) {
      err("project '%s' is missing the Channel3 field", filename.c_str());
      goto failed;
    }
    if (*value == 0) {
      project.channel3Filename = "";
    }
    else {
      project.channel3Filename = relativeToFile(filename, trim(value));
    }

    // The rest of the file is ignored, so you can have whatever content you
    // like in there, e.g. a description of the project.

    fclose(f);
    delete[] buf;
    return true;

  failed:
    fclose(f);
    delete[] buf;
    return false;
  }


  //
  // Command line handling
  //

  static void help(const char* progname)
  {
    info("Usage: %s [options] [file]", progname);
    info("");
    info("The <file> argument is the path to a shader file to run. If you omit it, we will");
    info("use a boring default shader instead.");
    info("");
    info("where [options] can be any valid combination of:");
    info("");
    info("  -n,--no-reload    Don't watch the shader file for changes. If you don't");
    info("                    specify this, the shader will be reloaded whenever it");
    info("                    changes.");
    info("  -p,--print-inputs Print the inputs that get supplied to your shader and exit.");
    info("  -s,--print-shader Print the default shader. This can be used as a starting");
    info("                    point for your own shaders.");
    info("  -c,--compat       Run in ShaderToy compatibility mode. This disables all");
    info("                    features that aren't also supported by ShaderToy. Use this");
    info("                    if you plan to upload your shader to ShaderToy eventually.");
    info("  -h,--help         Print this help info and exit.");
    info("  -v,--version      Print the program version number and exit.");
  }


  static void version()
  {
    info("ShaderTool 0.1");
    info("(c) Vilya Harvey, 2015");
  }


  static void printInputs()
  {
    info("%s", kFragShaderInputs);
  }


  static void printShader()
  {
    info("%s", kFragShaderBody);
  }


  static CommandLineResult parseCommandLine(int &argc, char **argv, Project &project)
  {
    // First, handle any options. Once they're handled we can remove them from
    // the argv array, which simplifies the logic for handling the non-option
    // args a lot.
    int i = 1;
    int j = 1;
    while (i < argc) {
      if (argv[i][0] == '-' && argv[i][1] == '-') {
        // Handle long-form options
        std::string arg(argv[i]);
        if (arg == "--no-reload") {
          gApp.reload = false;
          warn("option '%s' isn't implemented yet", argv[i]);
          ++i;
        }
        else if (arg == "--print-inputs") {
          printInputs();
          return eCleanExit;
        }
        else if (arg == "--print-shader") {
          printShader();
          return eCleanExit;
        }
        else if (arg == "--compat") {
          gApp.shadertoyCompatibility = true;
          warn("option '%s' isn't implemented yet", argv[i]);
          ++i;
        }
        else if (arg == "--help") {
          help(argv[0]);
          return eCleanExit;
        }
        else if (arg == "--version") {
          version();
          return eCleanExit;
        }
        else {
          err("unknown option '%s'", argv[i]);
          return eErrorExit;
        }
      }
      else if (argv[i][0] == '-') {
        // Handle short-form options
        switch(argv[i][1]) {
        case 'n':
          gApp.reload = false;
          warn("option '%s' isn't implemented yet", argv[i]);
          ++i;
          break;
        case 'p':
          printInputs();
          return eCleanExit;
        case 's':
          printShader();
          return eCleanExit;
        case 'c':
          gApp.shadertoyCompatibility = true;
          warn("option '%s' isn't implemented yet", argv[i]);
          ++i;
          break;
        case 'h':
          help(argv[0]);
          return eCleanExit;
        case 'v':
          version();
          return eCleanExit;
        default:
          err("unknown option '%s'", argv[i]);
          return eErrorExit;
        }
      }
      else {
        // Handle non-option arguments by moving them to the front of the argv
        // array.
        argv[j++] = argv[i++];
      }
    }
    argc = j;

    // By the time we get here, all the options have been handled and removed
    // from the command line, so that only the non-option arguments remain and
    // are bunched up contiguously at the front of the array. Now we need to
    // check that we have the expected number of them.
    if (argc > 2) {
      err("Expecting 1 argument, but found %d", argc - 1);
      return eErrorExit;
    }

    // We have the correct number of command line arguments, so now we can read
    // them. The first positional argument is at index 1, because index 0 still
    // holds the program name.
    if (argc == 1) {
      project.shaderFilename = "";
    }
    else if (fileExtension(argv[1]) == "stool") {
      // The file argument is a ShaderTool project file, so try to load it now.
      if (!loadProject(argv[1], project)) {
        return eErrorExit;
      }
    }
    else {
      // Otherwise, assume it's a fragment shader.
      project.shaderFilename = argv[1];
    }

    return eContinue;
  }


  //
  // GL-related bits
  //

  void printShaderError(const char* shaderName, GLuint shader, int bufSize, char* buf)
  {
    GLsizei length;
    glGetShaderSource(shader, bufSize, &length, buf);

    err("%s failed to compile", shaderName);

    std::string source(buf);
    std::string line;
    size_t lineStart = 0;
    size_t lineEnd = source.find('\n');
    int lineNum = 1;

    info("---------------------------");
    while (lineEnd != std::string::npos) {
      line = source.substr(lineStart, lineEnd - lineStart);
      info("%5d: %s", lineNum, line.c_str());
      lineStart = lineEnd + 1;
      lineEnd = source.find('\n', lineStart);
      ++lineNum;
    }
    line = source.substr(lineStart);
    info("%5d: %s", lineNum, line.c_str());
    info("---------------------------");
    info("");

    glGetShaderInfoLog(shader, bufSize, NULL, buf);
    info("compilation of %s failed with the following errors:\n%s", shaderName, buf);
  }


  bool usesFeedback(const char* fragShaderBody)
  {
    const char* pos = strstr(fragShaderBody, "mainImage");
    if (pos == NULL || *pos == '\0') {
      return false;
    }

    // Find the opening paren after the word "mainImage"
    pos = strstr(pos, "(");
    if (pos == NULL || *pos == '\0') {
      return false;
    }
    ++pos;

    // Count the number of parameters to the mainImage function.
    int numParams = 1;
    while (*pos != ')' && *pos != '\0') {
      if (*pos == ',') {
        ++numParams;
      }
      ++pos;
    }
    if (*pos != ')') {
      return false;
    }

    // If we have 3 parameters it should be the feedback version. If it's only
    // two, then it's the non-feedback version. If it's any other number then
    // it's an error, but we'll handle that by generating the non-feedback
    // version of the main function and letting the GL shader compiler report
    // the error.
    return numParams == 3;
  }


  bool shader(Shader& shader, const char* fragShaderBody, const char* fragShaderFilename)
  {
    const bool feedbackEnabled = usesFeedback(fragShaderBody);
    const GLchar* fragShaderSrcs[] = {
      kFragShaderVersion,
      kFragShaderInputs,
      kFragShaderPrefix,
      fragShaderBody,
      feedbackEnabled ? kFragShaderSuffixFeedbackOn : kFragShaderSuffixNoFeedback
    };
    const GLsizei numFragShaderSrcs = sizeof(fragShaderSrcs) / sizeof(fragShaderSrcs[0]);

    // TODO: check whether to enable the feedback texture by searching for the
    // mainImage declaration and parsing the parameters.

    // Set up the shaders.
    shader.vertexShader = glCreateShader(GL_VERTEX_SHADER);
    shader.fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    shader.program = glCreateProgram();
    glShaderSource(shader.vertexShader, 1, &kVertexShader, 0);
    glShaderSource(shader.fragmentShader, numFragShaderSrcs, fragShaderSrcs, 0);
    glCompileShader(shader.vertexShader);
    glCompileShader(shader.fragmentShader);
    glAttachShader(shader.program, shader.vertexShader);
    glAttachShader(shader.program, shader.fragmentShader);
    glLinkProgram(shader.program);

    // Check compilation & linking results
    GLint status;
    const int bufSize = 128 * 1024;
    char* buf = new char[bufSize];
    bool ok = true;

    glGetShaderiv(shader.vertexShader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
      printShaderError("vertex shader", shader.vertexShader, bufSize, buf);
      ok = false;
    }
    glGetShaderiv(shader.fragmentShader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
      printShaderError(fragShaderFilename, shader.fragmentShader, bufSize, buf);
      ok = false;
    }
    if (!ok) {
      delete[] buf;
      return false;
    }
    glGetProgramiv(shader.program, GL_LINK_STATUS, &status);
    if (status != GL_TRUE) {
      glGetProgramInfoLog(shader.program, bufSize, NULL, buf);
      err("program failed to link:\n%s", buf);
      ok = false;
    }
    if (!ok) {
      delete[] buf;
      return false;
    }

    // Look up the uniforms.
    glUseProgram(shader.program);
    shader.iResolution         = glGetUniformLocation(shader.program, "iResolution");
    shader.iGlobalTime         = glGetUniformLocation(shader.program, "iGlobalTime");
    shader.iChannelTime        = glGetUniformLocation(shader.program, "iChannelTime");
    shader.iChannelResolution  = glGetUniformLocation(shader.program, "iChannelResolution");
    shader.iMouse              = glGetUniformLocation(shader.program, "iMouse");
    shader.iChannel0           = glGetUniformLocation(shader.program, "iChannel0");
    shader.iChannel1           = glGetUniformLocation(shader.program, "iChannel1");
    shader.iChannel2           = glGetUniformLocation(shader.program, "iChannel2");
    shader.iChannel3           = glGetUniformLocation(shader.program, "iChannel3");
    shader.iChannelFeedback    = glGetUniformLocation(shader.program, "iChannelFeedback");
    shader.iDate               = glGetUniformLocation(shader.program, "iDate");
    shader.iSampleRate         = glGetUniformLocation(shader.program, "iSampleRate");

    delete[] buf;
    return true;
  }


  GLuint texture(const std::string& filename, float& w, float& h, float& components)
  {
    if (filename.empty()) {
      w = 1.0f;
      h = 1.0f;
      components = 1.0f;
      return 0;
    }

    GLuint handle = 0;
    int texWidth, texHeight, texComponents;
    unsigned char* pixels = NULL;
    GLenum targetFormat, sourceFormat, sourceType;

    if (stbi_is_hdr(filename.c_str())) {
      float* hdrpixels = stbi_loadf(filename.c_str(), &texWidth, &texHeight, &texComponents, 4);
      pixels = reinterpret_cast<unsigned char*>(hdrpixels);
      targetFormat = GL_RGBA32F;
      sourceFormat = GL_RGBA;
      sourceType = GL_FLOAT;
    }
    else {
      pixels = stbi_load(filename.c_str(), &texWidth, &texHeight, &texComponents, 4);
      targetFormat = GL_RGBA8;
      sourceFormat = GL_RGBA;
      sourceType = GL_UNSIGNED_BYTE;
    }

    if (pixels) {
      w = static_cast<float>(texWidth);
      h = static_cast<float>(texHeight);
      components = static_cast<float>(texComponents);

      glGenTextures(1, &handle);
      glBindTexture(GL_TEXTURE_2D, handle);

      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 12);
      glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

      glTexImage2D(GL_TEXTURE_2D, 0, targetFormat, texWidth, texHeight, 0, sourceFormat, sourceType, pixels);

      glGenerateMipmap(GL_TEXTURE_2D);
      stbi_image_free(pixels);
    }
    else {
      w = 1.0f;
      h = 1.0f;
      components = 1.0f;
    }

    return handle;
  }


  GLuint font(const std::string& filename, stbtt_bakedchar charDataOut[])
  {
    unsigned char* data = reinterpret_cast<unsigned char*>(loadFile(filename));
    if (data == NULL) {
      return 0;
    }

    const float fontHeightInPixels = 32.0f;
    const int mipLevel = 0;
    const int border = 0;

    unsigned char* bitmap = new unsigned char[kFontTexWidth * kFontTexHeight];
    stbtt_BakeFontBitmap(data, 0, fontHeightInPixels, bitmap, kFontTexWidth, kFontTexHeight, 32, 96, charDataOut);
    delete[] data;

    GLuint handle = 0;
    glGenTextures(1, &handle);
    glBindTexture(GL_TEXTURE_2D, handle);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, mipLevel, GL_RED, kFontTexWidth, kFontTexHeight, border, GL_RED, GL_UNSIGNED_BYTE, bitmap);

    delete[] bitmap;
    return handle;
  }


  void text(float x, float y, const char* fmt, ...)
  {
    // Assume the font shader & texture are already bound.

    // Generate the string that we're rendering.
    char buf[kMaxChars];
    va_list args;
    va_start(args, fmt);
    int len = vsnprintf(buf, kMaxChars, fmt, args);
    va_end(args);
    if (len >= kMaxChars) {
      len = kMaxChars - 1;
    }

    const int floatsPerVert = 4;
    const int vertsPerQuad = 6;
    const int xOfs = 0;
    const int yOfs = 1;
    const int uOfs = 2;
    const int vOfs = 3;

    glBindVertexArray(gApp.fontVAO);
    glBindBuffer(GL_ARRAY_BUFFER, gApp.fontBuf);

    float* vbo = reinterpret_cast<float*>(glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
    if (vbo == NULL) {
      return;
    }

    // Fill in the vbo with the data for this text.
    for (int i = 0; i < len; ++i) {
      stbtt_aligned_quad q;
      stbtt_GetBakedQuad(gApp.bakedChars, kFontTexWidth, kFontTexHeight, buf[i] - 32, &x, &y, &q, 1);

      // top left
      int v = i * floatsPerVert * vertsPerQuad;
      vbo[v + xOfs] = q.x0;
      vbo[v + yOfs] = q.y0;
      vbo[v + uOfs] = q.s0;
      vbo[v + vOfs] = q.t0;

      // bottom left
      v += floatsPerVert;
      vbo[v + xOfs] = q.x0;
      vbo[v + yOfs] = q.y1;
      vbo[v + uOfs] = q.s0;
      vbo[v + vOfs] = q.t1;

      // top right
      v += floatsPerVert;
      vbo[v + xOfs] = q.x1;
      vbo[v + yOfs] = q.y0;
      vbo[v + uOfs] = q.s1;
      vbo[v + vOfs] = q.t0;

      // bottom left
      v += floatsPerVert;
      vbo[v + xOfs] = q.x0;
      vbo[v + yOfs] = q.y1;
      vbo[v + uOfs] = q.s0;
      vbo[v + vOfs] = q.t1;

      // bottom right
      v += floatsPerVert;
      vbo[v + xOfs] = q.x1;
      vbo[v + yOfs] = q.y1;
      vbo[v + uOfs] = q.s1;
      vbo[v + vOfs] = q.t1;

      // top right
      v += floatsPerVert;
      vbo[v + xOfs] = q.x1;
      vbo[v + yOfs] = q.y0;
      vbo[v + uOfs] = q.s1;
      vbo[v + vOfs] = q.t0;
    }

    glUnmapBuffer(GL_ARRAY_BUFFER);

    // Draw the buffer using the font shader.
    glDrawArrays(GL_TRIANGLES, 0, vertsPerQuad * len);
  }


  bool setup(Project& project, int initialWidth, int initialHeight)
  {
    glDebugMessageCallback(errorHandlerGL, NULL);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, GL_FALSE);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

    // Load the channel textures
    project.texChannel0 = texture(project.channel0Filename, project.channelResolution[0], project.channelResolution[1], project.channelResolution[2]);
    project.texChannel1 = texture(project.channel1Filename, project.channelResolution[3], project.channelResolution[4], project.channelResolution[5]);
    project.texChannel2 = texture(project.channel2Filename, project.channelResolution[6], project.channelResolution[7], project.channelResolution[8]);
    project.texChannel3 = texture(project.channel3Filename, project.channelResolution[9], project.channelResolution[10], project.channelResolution[11]);

    // Create the render target textures
    glGenTextures(1, &project.outputTex);
    glGenTextures(2, project.feedbackTex);
    glBindTexture(GL_TEXTURE_2D, project.outputTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, initialWidth, initialHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, (const GLvoid*)0);
    for (int i = 0; i < 2; ++i) {
      glBindTexture(GL_TEXTURE_2D, project.feedbackTex[i]);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, initialWidth, initialHeight, 0, GL_RGBA, GL_FLOAT, (const GLvoid*)0);
    }
    glBindTexture(GL_TEXTURE_2D, 0);

    // Create and clear the framebuffers
    glGenFramebuffers(1, &project.fbo);
    const float initialFeedback[4] = { 0.0, 0.0, 0.0, 0.0 };
    glBindFramebuffer(GL_FRAMEBUFFER, project.fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, project.outputTex, 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, project.feedbackTex[1], 0);
    GLenum framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (framebufferStatus != GL_FRAMEBUFFER_COMPLETE) {
      err("framebuffer is incomplete");
      return false;
    }
    const GLenum drawBuffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers(2, drawBuffers);
    glClearBufferfv(GL_COLOR, 1, initialFeedback);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, project.feedbackTex[0], 0);
    glClearBufferfv(GL_COLOR, 1, initialFeedback);

    // Load the shader file, if one has been specified.
    const char* fragmentShaderBody = kFragShaderBody;
    if (!project.shaderFilename.empty()) {
      fragmentShaderBody = loadFile(project.shaderFilename);
    }

    // It should only be possible for fragmentShaderBody to be NULL if we tried
    // to load a file and failed.
    if (fragmentShaderBody == NULL) {
      return false;
    }

    // Set up the user shader
    bool ok = shader(project.userShader, fragmentShaderBody, project.shaderFilename.empty() ? "default shader" : project.shaderFilename.c_str());
    if (fragmentShaderBody != kFragShaderBody) {
      delete[] fragmentShaderBody;
    }
    if (!ok) {
      return false;
    }

    // Create a VAO.
    glGenVertexArrays(1, &project.vao);
    glBindVertexArray(project.vao);
    glEnableVertexAttribArray(0);

    // Create the vertex buffer & tell the VAO about it.
    glGenBuffers(1, &project.vertexBuf);
    glBindBuffer(GL_ARRAY_BUFFER, project.vertexBuf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 2 * 4, kQuadVerts, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

    // --- Setup for text drawing ---

    // Set up the font texture
    gApp.fontTex = font("fonts/Prototype.ttf", gApp.bakedChars);

    // Set up the font shaders
    gApp.fontVertexShader = glCreateShader(GL_VERTEX_SHADER);
    gApp.fontFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    gApp.fontProgram = glCreateProgram();
    glShaderSource(gApp.fontVertexShader, 1, &kFontVertexShader, 0);
    glShaderSource(gApp.fontFragmentShader, 1, &kFontFragmentShader, 0);
    glCompileShader(gApp.fontVertexShader);
    glCompileShader(gApp.fontFragmentShader);
    glAttachShader(gApp.fontProgram, gApp.fontVertexShader);
    glAttachShader(gApp.fontProgram, gApp.fontFragmentShader);
    glLinkProgram(gApp.fontProgram);

    // Check compilation and linking results.
    GLint status;
    const int bufSize = 128 * 1024;
    char* buf = new char[bufSize];
    glGetShaderiv(gApp.fontVertexShader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
      glGetShaderInfoLog(gApp.fontVertexShader, bufSize, NULL, buf);
      err("font vertex shader failed to compile:\n%s", buf);
      ok = false;
    }
    glGetShaderiv(gApp.fontFragmentShader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
      glGetShaderInfoLog(gApp.fontFragmentShader, bufSize, NULL, buf);
      err("font fragment shader failed to compile:\n%s", buf);
      ok = false;
    }
    if (!ok) {
      delete[] buf;
      return false;
    }
    glGetProgramiv(gApp.fontProgram, GL_LINK_STATUS, &status);
    if (status != GL_TRUE) {
      glGetProgramInfoLog(gApp.fontProgram, bufSize, NULL, buf);
      err("font program failed to link:\n%s", buf);
      ok = false;
    }
    if (!ok) {
      delete[] buf;
      return false;
    }
    delete[] buf;

    // Look up the uniforms
    glUseProgram(gApp.fontProgram);
    gApp.iResolution  = glGetUniformLocation(gApp.fontProgram, "iResolution");
    gApp.iFontTex     = glGetUniformLocation(gApp.fontProgram, "iFontTex");
    gApp.iFontColor   = glGetUniformLocation(gApp.fontProgram, "iFontColor");

    // Create a VAO & buffer for drawing text with. The VAO has pos and uv
    // attributes, stored as interleaved vec2 values. Each quad is stored as a
    // pair of triangles (6 verts), so we can draw the whole thing in a single
    // call without needing an index array.
    const int vertsPerQuad = 6;
    const int floatsPerVert = 4;
    glGenVertexArrays(1, &gApp.fontVAO);
    glBindVertexArray(gApp.fontVAO);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &gApp.fontBuf);
    glBindBuffer(GL_ARRAY_BUFFER, gApp.fontBuf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * floatsPerVert * vertsPerQuad * kMaxChars, NULL, GL_STREAM_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, floatsPerVert * sizeof(float), (GLvoid*)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, floatsPerVert * sizeof(float), (GLvoid*)(2 * sizeof(float)));

    return true;
  }


  void update(GLFWwindow* window, Project& project)
  {
    int w, h;
    glfwGetWindowSize(window, &w, &h);

    if (w != static_cast<int>(project.resolution[0]) || h != static_cast<int>(project.resolution[1])) {
      // TODO: reallocate all the render targets
      // TODO: clear the render targets
      // TODO: reset the playback time to 0
    }

    project.resolution[0] = static_cast<float>(w);
    project.resolution[1] = static_cast<float>(h);
    project.resolution[2] = 32.0f;

    double now = tick();
    if (project.paused != project.wasPaused) {
      // If we're pausing
      if (project.paused) {
        project.globalTimeStop = now;
      }
      // Otherwise, we're resuming
      else {
        project.globalTimeStart = now - project.globalTimeStop + project.globalTimeStart;
      }
      project.wasPaused = project.paused;
    }

    bool shiftDown = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS ||
                     glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS;
    bool altDown = glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS ||
                   glfwGetKey(window, GLFW_KEY_RIGHT_ALT) == GLFW_PRESS;
    double offset = 1.0;
    if (shiftDown && !altDown) {
      offset *= 0.1;
    }
    if (altDown && !shiftDown) {
      offset *= 10.0;
    }

    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
      project.globalTimeOffset -= offset;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
      project.globalTimeOffset += offset;
    }

    if (!project.paused) {
      project.globalTimeStart -= project.globalTimeOffset;
      project.globalTime = static_cast<float>(now - project.globalTimeStart);
    }
    else {
      project.globalTimeStop += project.globalTimeOffset;
      project.globalTime = static_cast<float>(project.globalTimeStop - project.globalTimeStart);
    }
    project.globalTimeOffset = 0.0;

    for (int i = 0; i < 4; ++i) {
      project.channelTime[i] = project.globalTime;
      project.channelResolution[i * 3    ] = project.resolution[0];
      project.channelResolution[i * 3 + 1] = project.resolution[1];
      project.channelResolution[i * 3 + 2] = project.resolution[2];
    }

    int button0State = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (button0State == GLFW_PRESS) {
      double mx, my;
      glfwGetCursorPos(window, &mx, &my);
      project.mouseX = static_cast<float>(mx);
      project.mouseY = static_cast<float>(my);
    }
    else {
      project.clickX = project.mouseX;
      project.clickY = project.mouseY;
    }

    project.year = 2015.0f;
    project.month = 2.0f;
    project.day = 10.0f;
    project.timeInSecs = 0.0f;
  }


  void runShader(const Project& project, const Shader& shader)
  {
    // Bind the shader
    glUseProgram(shader.program);

    const GLenum drawBuffers[2] = {
      GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1
    };
    glDrawBuffers(2, drawBuffers);

    // Set uniforms
    glUniform3fv(shader.iResolution, 1, project.resolution);
    glUniform1f(shader.iGlobalTime, project.globalTime);
    glUniform1fv(shader.iChannelTime, 4, project.channelTime);
    glUniform3fv(shader.iChannelResolution, 4, project.channelResolution);
    glUniform4f(shader.iMouse, project.mouseX, project.mouseY, project.clickX, project.clickY);
    glUniform1i(shader.iChannel0, 0);
    glUniform1i(shader.iChannel1, 1);
    glUniform1i(shader.iChannel2, 2);
    glUniform1i(shader.iChannel3, 3);
    glUniform1i(shader.iChannelFeedback, 4);
    glUniform4f(shader.iDate, project.year, project.month, project.day, project.timeInSecs);
    glUniform1f(shader.iSampleRate, project.audioSampleRate);

    // Draw
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  }


  void draw(Project& project)
  {
    const GLsizei w = static_cast<GLsizei>(project.resolution[0]);
    const GLsizei h = static_cast<GLsizei>(project.resolution[1]);
    glViewport(0, 0, w, h);

    // Bind the vertex buffer.
    glBindVertexArray(project.vao);

    // Bind the textures.
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, project.texChannel0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, project.texChannel1);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, project.texChannel2);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, project.texChannel3);

    // Bind the feedback texture
    int feedbackSrc = project.currFrameTime % 2;
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, project.feedbackTex[feedbackSrc]);

    // Bind the framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, project.fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, project.feedbackTex[1 - feedbackSrc], 0);

    // Set up GL state
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    // Draw
    runShader(project, project.userShader);

    // Blit the color output from the current framebuffer to the screen.
    glBindFramebuffer(GL_READ_FRAMEBUFFER, project.fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, w, h, 0, 0, w, h, GL_COLOR_BUFFER_BIT, GL_NEAREST);

    // -- Now we may need to draw the text overlays, so switch to the font shader --
    if (gApp.displayOverlay) {
      // Bind the shader
      glUseProgram(gApp.fontProgram);

      // Bind the vertex buffer
      glBindVertexArray(gApp.fontVAO);

      // Bind the texture
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, gApp.fontTex);

      // Set uniforms
      glUniform2fv(gApp.iResolution, 1, project.resolution);
      glUniform1i(gApp.iFontTex, 0);
      glUniform4f(gApp.iFontColor, 1.0f, 0.3f, 0.3f, 1.0f);

      // Set up GL state
      glEnable(GL_BLEND);
      glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

      // Draw the current global time value.
      text(project.resolution[0] / 2.0f - 32.0f, -8.0f, "Time: %4.2f", project.globalTime);

      // Draw the current frame rate.
      double secsPerFrame = project.frameTimesSum / kMaxFrameTimes;
      double framesPerSec = 1.0 / secsPerFrame;
      text(8.0f, -40.0f, "%4.2lf ms/frame", secsPerFrame * 1000.0);
      text(8.0f,  -8.0f, "%4.2lf fps",      framesPerSec);
    }
  }


  void teardownShader(Shader& shader)
  {
    glDeleteProgram(shader.program);
    glDeleteShader(shader.vertexShader);
    glDeleteShader(shader.fragmentShader);
  }


  void teardown(Project& project)
  {
    // Delete the shaders
    teardownShader(project.userShader);

    // Delete the VAOs.
    glDeleteVertexArrays(1, &project.vao);
    glDeleteVertexArrays(1, &gApp.fontVAO);

    // Delete the vertex buffers
    glDeleteBuffers(1, &project.vertexBuf);
    glDeleteBuffers(1, &gApp.fontBuf);

    // Delete the textures.
    glDeleteTextures(1, &gApp.fontTex);
    glDeleteTextures(1, &project.texChannel0);
    glDeleteTextures(1, &project.texChannel1);
    glDeleteTextures(1, &project.texChannel2);
    glDeleteTextures(1, &project.texChannel3);

    // Delete the render target textures.
    glDeleteTextures(1, &project.outputTex);
    glDeleteTextures(2, project.feedbackTex);

    // Delete the framebuffers.
    glDeleteFramebuffers(1, &project.fbo);
  }

} // namespace vh


int main(int argc, char** argv)
{
  vh::Project project;

  switch (vh::parseCommandLine(argc, argv, project)) {
  case vh::eCleanExit:
    return 0;
  case vh::eErrorExit:
    return 1;
  default:
    break;
  }

  glfwSetErrorCallback(vh::errorHandlerGLFW);
  if (!glfwInit()) {
    vh::err("GLFW failed to initialise");
    return 1;
  }

  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

  GLFWwindow* window = glfwCreateWindow(1280, 640, "ShaderTool", NULL, NULL);
  if (!window) {
    vh::err("unable to create window");
    glfwTerminate();
    return 1;
  }
  glfwSetWindowUserPointer(window, &project);

  glfwMakeContextCurrent(window);
  if (ogl_LoadFunctions() == ogl_LOAD_FAILED) {
    vh::err("failed to load OpenGL function pointers");
    glfwDestroyWindow(window);
    glfwTerminate();
    return 1;
  }

  glfwSetKeyCallback(window, vh::keyHandlerGLFW);
  glfwSetFramebufferSizeCallback(window, vh::resizeHandlerGLFW);

  int initialWidth, initialHeight;
  glfwGetWindowSize(window, &initialWidth, &initialHeight);

  bool ok = vh::setup(project, initialWidth, initialHeight);
  if (!ok) {
    vh::err("setup failed");
    glfwDestroyWindow(window);
    glfwTerminate();
    return 1;
  }

  project.globalTimeStart = vh::tick();
  while (!glfwWindowShouldClose(window)) {
    project.frameTimesSum -= project.frameTimes[project.currFrameTime];
    project.frameTimes[project.currFrameTime] = vh::tick();

    vh::update(window, project);
    vh::draw(project);

    project.frameTimes[project.currFrameTime] = vh::tick() - project.frameTimes[project.currFrameTime];
    project.frameTimesSum += project.frameTimes[project.currFrameTime];
    project.currFrameTime = (project.currFrameTime + 1) % vh::kMaxFrameTimes;

    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  vh::teardown(project);
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}
